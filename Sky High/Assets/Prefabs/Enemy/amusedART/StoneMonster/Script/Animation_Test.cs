﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_Test : MonoBehaviour
{
    private bool initialized = false;           //if the class' variables have been initialized

    public const string IDLE = "Anim_Idle";
    public const string RUN = "Anim_Run";
    public const string ATTACK = "Anim_Attack";
    public const string DAMAGE = "Anim_Damage";
    public const string DEATH = "Anim_Death";

    Animation anim;
    public float timeBetweenAttacks = 1.5f;     // The time in seconds between each attack.
    public float attackDamage = 0.5f;               // The amount of health taken away per attack.

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerHP;                          // Reference to the player's health.
    HealthBase enemyHP;                          // Reference to this enemy's health.
    enemyAttack enemyA;
    public bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    float timer;

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        anim = GetComponent<Animation>();
        enemyHP = gameObject.GetComponent<enemyHP>();
        enemyA = GetComponent<enemyAttack>();
        initialized = true;
    }

    void OnTriggerEnter(Collider other)
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // If the entering collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is in range.
            playerInRange = true;
        }

    }

    void OnTriggerExit(Collider other)
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // If the exiting collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is no longer in range.
            playerInRange = false;
        }
    }

    public void Update()
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && playerInRange && enemyHP.currentHealth > 0)
        {
            // ... attack.
            Attack();
        }

        // If the player has zero or less health...
        else if (playerHP.currentHealth <= 0)
        {
            // ... tell the animator the player is dead.
            anim.Play(IDLE);
            //IdleAni();
        }

        else if (enemyHP.currentHealth <= 0 )
        {
            anim.Play(IDLE);
        }
    }

    public void Attack()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
        if (playerHP.currentHealth > 0)
        {
            AttackAni();          
            anim.Play(ATTACK);
            // ... damage the player.
            playerHP.takeDamage(playerHP.currentEDamage);
        }
    }

    public void IdleAni()
    {
        anim.CrossFade(IDLE);
    }

    public void RunAni()
    {
        anim.CrossFade(RUN);
    }

    public void AttackAni()
    {
        anim.CrossFade(ATTACK);
    }

    public void DamageAni()
    {
        anim.CrossFade(DAMAGE);
    }

    public void DeathAni()
    {
        anim.CrossFade(DEATH);
    }
}