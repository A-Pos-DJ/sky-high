﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnFromPool : MonoBehaviour
{
    public string enemyTag;         //Enemy's tag in the object pool
    public int spawnLimit;          //Max enemies to spawn before stopping
    public float timeBtwnSpawns;        //Amount of time between spawns
    public float waves = 1;         //How many times this spawner will spawn enemies 
    public float timeBtwnWaves;         //Amount of time before spawning another wave of enemies 
    public float spawnRange;        //Max distance from spawner that enemies will spawn


    private bool spawning = false;
    private float timer = 0;
    private float spawnCount = 0;
    private float waveCount = 0;
    private Transform origin;
    private ObjectPooler objectPool;
    // Start is called before the first frame update
    void Start()
    {
        origin = transform;
        objectPool = ObjectPooler.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (waveCount < (waves - 1) && timer >= timeBtwnWaves)
        {
            spawnCount = 0;
        }

        if(spawnCount < spawnLimit && !spawning)
        {
            StartCoroutine(SpawnEnemies());
        }
        else
        {
            timer += Time.deltaTime;
        }
    }

    IEnumerator SpawnEnemies()
    {
        spawning = true;
        for(; spawnCount < spawnLimit; spawnCount++)
        {
            Vector3 spawnPoint = new Vector3(origin.position.x + Random.Range(-spawnRange, spawnRange), 0f, origin.position.z + Random.Range(-spawnRange, spawnRange));
            objectPool.SpawnFromPool(enemyTag, spawnPoint, Quaternion.identity);
            yield return new WaitForSeconds(timeBtwnSpawns);
        }
        spawning = false;
    }
}
