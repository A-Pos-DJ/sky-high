﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (GetComponentInParent<PierceShotScript>() == null)
            return;

        ProjectileShotBase shot = GetComponentInParent<ProjectileShotBase>();

        if (other.tag == "Player")
        {
            PlayerHP playerHP = other.GetComponentInParent<PlayerHP>();

            // If the player has health to lose...
            if (playerHP.currentHealth > 0)
            {
                // ... damage the player.
                playerHP.takeDamage(shot.damage);
            }

            shot.DeactivatePrefab();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("SolidObject"))
        {
                shot.DeactivatePrefab();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
                shot.DeactivatePrefab();
        }
    }
}
