﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RoomNode : MonoBehaviour
{
    public class NodeCost : IComparable
    {
        public Coordinates coordinates;
        //Total cost so far for the node
        public float gCost;
        //Estimated cost from this node to the goal node
        public float hCost;
        //parent node when it comes to finding paths
        public NodeCost parent;


        public NodeCost(Coordinates incomingCoordinates)
        {
            coordinates = incomingCoordinates;
            hCost = 0.0f;
            gCost = 1.0f;
            parent = null;
        }

        //clears the cost back to the original values
        public void Clear()
        {
            hCost = 0.0f;
            gCost = 1.0f;
            parent = null;
        }

        //IComparable Interface method implementation
        public int CompareTo(object obj)
        {
            NodeCost node = (NodeCost)obj;
            if (hCost < node.hCost)
            {
                return -1;
            }
            if (hCost > node.hCost)
            {
                return 1;
            }
            return 0;
        }
    }

    public bool debugMode = false;
    bool initalized = false;

    public RoomType roomType;
    public NodeCost cost;
    public Coordinates coordinates;
    public Dictionary<Direction, WallNode> WallConnector;
    GameObject miniMapIcon;
    GameObject roomLights;
    GameObject enemySpawnPoints;
    GameObject itemSpawnPoints;
    List<GameObject> roomEnemies;
    List<GameObject> roomItems;

    bool roomDiscovered;
    bool roomActive;
    bool roomCleared;

    public void Initilize(Coordinates coordinatesInput, RoomType roomTypeInput)
    {
        coordinates = coordinatesInput;
        roomType = roomTypeInput;
        WallConnector = new Dictionary<Direction, WallNode>();
        cost = new NodeCost(coordinatesInput);
        miniMapIcon = gameObject.transform.GetChild(0).gameObject;
        roomLights = gameObject.transform.GetChild(1).gameObject;
        enemySpawnPoints = gameObject.transform.GetChild(2).gameObject;
        itemSpawnPoints = gameObject.transform.GetChild(3).gameObject;
        roomDiscovered = false;
        MiniMapIconSetActive(false);
        DeactivateRoom();
        roomCleared = false;
        roomEnemies = new List<GameObject>();
        roomItems = new List<GameObject>();
        initalized = true;
    }

    //activates the room so that the player is able to interact and see what is in it
    public void ActivateRoom()
    {
        roomActive = true;

        //if the room has not already been discovered, it will now be on the minimap
        if (!roomDiscovered)
        {
            roomDiscovered = true;
            MiniMapIconSetActive(true);
        }

        roomLights.SetActive(true);
        SetListActive(roomEnemies, true);
        SetListActive(roomItems, true);

        //may end up moving this method to the specific enemies that get defeated
        CheckIfRoomCleared();
    }

    //deactiavtes the room so that the player cannnot see or interact with the object in it
    public void DeactivateRoom()
    {
        //set all objects in the room as inactive
        roomActive = false;
        roomLights.SetActive(false);
        SetListActive(roomEnemies, false);
        SetListActive(roomItems, false);
    }


    //generates enemies for the room based on the passed in enemy prefabs
    public void GenerateRoomEnemies(List<GameObject> enemyPrefabs, int numberOfEnemiesNeeded)
    {
        GenerateRoomPrefabObjects(GenerateObjectType.ENEMY, enemyPrefabs, numberOfEnemiesNeeded, true, true);
    }

    //generates objects for a room based on the passed in variables
    public void GenerateRoomPrefabObjects(GenerateObjectType objectType, List<GameObject> objectPrefabs,
        int numberOfObjectsNeeded, bool offsetSpawnPoint, bool spawnTheSameObject)
    {
        List<GameObject> roomObjectList;
        GameObject objectSpawnPoints;

        //set the room object list and the object spawn point list based on which object type is being spawned
        switch (objectType)
        {
            case GenerateObjectType.BOSS:
                roomObjectList = roomEnemies;
                objectSpawnPoints = enemySpawnPoints;
                break;
            case GenerateObjectType.ENEMY:
                roomObjectList = roomEnemies;
                objectSpawnPoints = enemySpawnPoints;
                break;
            case GenerateObjectType.ITEM:
                roomObjectList = roomItems;
                objectSpawnPoints = itemSpawnPoints;
                break;
            case GenerateObjectType.POWERUP:
                roomObjectList = roomItems;
                objectSpawnPoints = itemSpawnPoints;
                break;
            default:
                Debug.Log("Error: could not generate room objects for object type: " + objectType.ToString());
                return;
        }


        int offset = 0;
        //get a random object prefab and set the prefab to be spawned
        GameObject prefabToBeSpawned = objectPrefabs[UnityEngine.Random.Range(0, objectPrefabs.Count)];

        //if we want to offset the spawn, set the offset to be a random spawn point
        if (offsetSpawnPoint)
            UnityEngine.Random.Range(0, objectSpawnPoints.transform.childCount);

        //while we have not spawned enough objects that are needed...
        while (roomObjectList.Count < numberOfObjectsNeeded)
        {
            //loop though what is left in the spawn point list, then start over from the beginning of the spawn point list
            for (int index = 0; index < objectSpawnPoints.transform.childCount; index++)
            {
                index += offset;
                offset = 0;

                //if we have not spawned enough objects yet
                if (roomObjectList.Count < numberOfObjectsNeeded)
                {
                    //if we do not want the same object to be spawned in this room, get another random object
                    if (!spawnTheSameObject)
                        prefabToBeSpawned = objectPrefabs[UnityEngine.Random.Range(0, objectPrefabs.Count)];

                    //Generate the object based on the object type
                    switch (objectType)
                    {
                        case GenerateObjectType.BOSS:
                            GameObject newBoss = InstantiateRoomObject(GenerateObjectType.BOSS, 
                                prefabToBeSpawned, objectSpawnPoints.transform.GetChild(index).transform);
                            newBoss.GetComponent<BossHandler>().DeactivateBoss();
                            break;
                        case GenerateObjectType.ENEMY:
                            GameObject newEnemy = InstantiateRoomObject(GenerateObjectType.ENEMY,
                                prefabToBeSpawned, objectSpawnPoints.transform.GetChild(index).transform);
                            newEnemy.GetComponent<EnemyHandler>().DeactivateEnemy();
                            break;
                        case GenerateObjectType.ITEM:
                            GameObject newItem = InstantiateRoomObject(GenerateObjectType.ITEM,
                                prefabToBeSpawned, objectSpawnPoints.transform.GetChild(index).transform);
                            newItem.SetActive(false);
                            break;
                        case GenerateObjectType.POWERUP:
                            GameObject newPowerUp = InstantiateRoomObject(GenerateObjectType.POWERUP,
                                prefabToBeSpawned, objectSpawnPoints.transform.GetChild(index).transform);
                            newPowerUp.SetActive(false);
                            break;
                        default:
                            Debug.Log("Error: could not generate room objects for object type: " + objectType.ToString());
                            return;
                    }
                }
                else //if we have enough objects, break the loop
                {
                    break;
                }
            }
        }
    }

    //create the gameObject, then add it to the passed in list
    GameObject InstantiateRoomObject(GenerateObjectType objectType, GameObject objectPrefab, Transform spawnTransform)
    {
        GameObject newObject = Instantiate(objectPrefab, spawnTransform);
        //Generate the object based on the object type
        switch (objectType)
        {
            case GenerateObjectType.BOSS:
                newObject.GetComponent<BossHandler>().Init(this, spawnTransform.position);
                roomEnemies.Add(newObject);
                break;
            case GenerateObjectType.ENEMY:
                newObject.GetComponent<EnemyHandler>().Init(this, spawnTransform.position);
                roomEnemies.Add(newObject);
                break;
            case GenerateObjectType.ITEM:
                newObject.GetComponent<ItemHandler>().Init(this, spawnTransform.position);
                roomItems.Add(newObject);
                break;
            case GenerateObjectType.POWERUP:
                newObject.GetComponent<ItemHandler>().Init(this, spawnTransform.position);
                roomItems.Add(newObject);
                break;
            default:
                Debug.Log("Error: could not instantiate room object for object type: " + objectType.ToString());
                break;
        }
        return newObject;
    }

    //create the dropped item, then add it to the room item list
    public GameObject InstantiateDroppedItem(GameObject objectPrefab, Vector3 dropPosition)
    {
        Transform dropTransform = itemSpawnPoints.transform;
        dropTransform.position = new Vector3(dropPosition.x, 0.2f, dropPosition.z);
        dropTransform.rotation = new Quaternion(0, 0, 0, 0);

        GameObject newDroppedItem = Instantiate(objectPrefab, dropTransform);
        newDroppedItem.GetComponent<ItemHandler>().Init(this, dropTransform.position);
        //Generate the object based on the object type
        roomItems.Add(newDroppedItem);
        return newDroppedItem;
    }

    //removed the enemy from the enemy list
    public void RemoveEnemy(GameObject enemyBeingDestroyed)
    {
        roomEnemies.Remove(enemyBeingDestroyed);
        CheckIfRoomCleared();
    }

    //removes the item from the item list
    public void RemoveItem(GameObject itemBeingDestroyed)
    {
        roomItems.Remove(itemBeingDestroyed);
    }

    //checks to see if there is anymore enemies in the room
    public void CheckIfRoomCleared()
    {
        if (roomEnemies.Count == 0)
        {
            SetRoomAsCleared();
        }
        else
            roomCleared = false;
    }

    //sets the minimap icon to be true or false depending on the argument
    public void MiniMapIconSetActive(bool activator)
    {
        miniMapIcon.SetActive(activator);
        foreach (KeyValuePair<Direction, WallNode> connector in WallConnector)
        {
            connector.Value.MiniMapIconSetActive(activator);
        }
    }

    //sets all objects in the passed in list as active or inactive
    void SetListActive(List<GameObject> objectList, bool activator)
    {
        if (objectList == null || objectList.Count == 0)
            return;

        if (objectList == roomEnemies)
        {
            foreach (GameObject enemy in objectList)
            {
                if (enemy.GetComponent<EnemyHandler>() != null)
                {
                    if (activator)
                        enemy.GetComponent<EnemyHandler>().ActivateEnemy();
                    else if (!activator)
                        enemy.GetComponent<EnemyHandler>().DeactivateEnemy();
                }
                if (enemy.GetComponent<BossHandler>() != null)
                {
                    if (activator)
                        enemy.GetComponent<BossHandler>().ActivateBoss();
                    else if (!activator)
                        enemy.GetComponent<BossHandler>().DeactivateBoss();
                }
            }
        }
        else if (objectList == roomItems)
        {
            if (!roomCleared && activator)
                return;

            foreach (GameObject obj in objectList)
            {
                obj.SetActive(activator);
            }
        }
        else
        {
            foreach (GameObject obj in objectList)
            {
                obj.SetActive(activator);
            }
        }
    }

    //creates a wall at the space specified, returns false if a wall cannot be generated
    public bool GenerateWall(Direction direction, WallNode wallnode)
    {
        if (WallConnector.ContainsKey(direction))
        {
            if (WallConnector[direction].wallType == WallType.WALL)
            {
                /*
                Debug.Log("Replacing wall at " + coordinates.ToString() +
                    "\nin the direction:" + direction.ToString() +
                    "\nwith the type of: " + wallnode.wallType.ToString());
                */

                WallNode nodeBeingRemoved = WallConnector[direction];
                WallConnector.Remove(direction);
                Destroy(nodeBeingRemoved.gameObject);
                return true;
            }
            else
            {
                /*
                Debug.Log("ERROR: wall at " + coordinates.ToString() +
                        "\nin the direction:" + direction.ToString() +
                        "\nwallNode already exists cannot replaced");
                */
                return false;
            }
        }

        //add the wall connector to the room
        WallConnector.Add(direction, wallnode);
        return true;
    }


    //the room is cleared. Unlock all of the doors and maybe give the player something
    void SetRoomAsCleared()
    {
        //if the room has not been cleared prior to checking
        if (!roomCleared)
        {
            //add in reward for clearing room here

            switch (roomType)
            {
                case RoomType.NORMAL:
                    //stuff
                    break;
                case RoomType.START:
                    //nothing happens here... its the start room
                    break;
                case RoomType.ITEM:
                    //you get an item or already got one?... I dont think enemies will be in this room
                    break;
                case RoomType.SHOP:
                    //you get to shop... i dont think enemies will be in this room either
                    break;
                case RoomType.BOSS:
                    //you get an item for defeating the boss... creates a portal for the next level
                    CreatePortalToNextFloor();
                    break;

                default:
                    Console.WriteLine("Unknown Room Type:" + roomType.ToString() + "room set as cleared");
                    break;
            }
            roomCleared = true;
            //set the room items to be active
            SetListActive(roomItems, true);
        }
        UnlockBarredDoors();
    }

    public GameObject GetEnemySpawnPoints()
    { return enemySpawnPoints; }

    //unlocks all barred doors in the room
    void UnlockBarredDoors()
    {
        foreach (KeyValuePair<Direction, WallNode> connector in WallConnector)
        {
            connector.Value.UnlockDoor(WallType.BARS);
        }
    }

    //create a portal in the room and add it to the list of items 
    void CreatePortalToNextFloor()
    {
        roomItems.Add(Instantiate(PrefabContainer.instance.portal, transform));
    }

    //script that generates items for a room
    void GenerateItems()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

    //script that generates NPCs for the room
    void GenerateNPCs()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

}
