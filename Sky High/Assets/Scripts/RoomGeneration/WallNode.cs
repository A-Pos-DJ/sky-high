﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallNode : MonoBehaviour
{
    public WallType wallType;
    public WallNode wallLink;
    [HideInInspector] private GameObject barsRig;
    [HideInInspector] public GameObject miniMapIcon;
    bool isLocked = false;

    //initilize the wall node if there is not already a walltype declared
    public void Init(WallType wallTypeInput)
    {
        wallType = wallTypeInput;

        if (wallTypeInput == WallType.BARS)
            isLocked = true;

        miniMapIcon = gameObject.transform.GetChild(0).gameObject;
        barsRig = gameObject.transform.GetChild(2).gameObject;
        MiniMapIconSetActive(false);
    }


    //sets the minimap icon to be true or false depending on the argument
    public void MiniMapIconSetActive(bool activator)
    {
        miniMapIcon.SetActive(activator);
    }

    //links this node and another wall node together
    public void WallLink(WallNode linkNode)
    {
        wallLink = linkNode;

        //ensure we do not go into an endless loop function by adding an if statment
        if (linkNode.wallLink == null)
            linkNode.WallLink(this);
    }

    public void WallLink(GameObject linkNodeObject)
    {
        wallLink = linkNodeObject.GetComponent<WallNode>();

        //ensure we do not go into an endless loop function by adding an if statment
        if(linkNodeObject.GetComponent<WallNode>().wallLink == null)
            linkNodeObject.GetComponent<WallNode>().WallLink(this);
    }

    //unlocks a specific doortype based on the input
    public void UnlockDoor(WallType unlockType)
    {
        if (!isLocked)
            return;

        if (unlockType == wallType)
        {
            isLocked = false;
            barsRig.transform.position = new Vector3(0, -2, 0);
            miniMapIcon.GetComponent<SpriteRenderer>().color = Color.black;
        }
    }

    /*
    //disables the player then transitions him from one room to another
    IEnumerator LowerTheBars()
    {
        float percentCompleted = 0f;
        float elapsedTime = 0;
        float waitTime = 0.75f;
        Vector3 startPosition = new Vector3(0, 0, 0);
        Vector3 targetPosition = new Vector3(0, -2, 0);

        while (elapsedTime <= waitTime)
        {
            if (barsRig == null)
                break;

            Debug.Log(percentCompleted);

            percentCompleted = Mathf.Clamp01(elapsedTime / waitTime);
            barsRig.transform.position = Vector3.Lerp(startPosition, targetPosition, percentCompleted);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
    */

    private void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.CompareTag("Player"))
        {
            if(!isLocked)
                GameHandler.instance.RoomTransition(wallLink);
        }
    }

}
