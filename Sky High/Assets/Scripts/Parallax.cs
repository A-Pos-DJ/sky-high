﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject image;
    public float speedCoefficient;
    public Transform end;
    public Transform start;

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, end.position, speedCoefficient * Time.deltaTime);

        if (Vector3.Distance(transform.position, end.position) < 0.1f)
        {
            transform.position = start.position;
        }
    }
}
