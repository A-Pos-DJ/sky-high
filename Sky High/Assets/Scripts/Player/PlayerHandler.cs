﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandler : MonoBehaviour
{
    public bool debugMode = false;

    //player stats mostly set by the game handler
    public float hpCurrent = 0f;
    protected int hpMax = 0;
    public float walkSpeed = 0f;
    public float runSpeed = 0f;
    public float sprintSpeed = 0f;

    public bool lockMovement = true;
    public bool canSprint = false;

    Animator anim;
    bool damaged;
    bool isDead;

    GameObject playerRig;
    GameObject playerAura;


    //in case we are just looking to test out the player
    private void Start()
    {
        if (debugMode)
        {
            Initilize(10, 2.5f, 3f, 3.5f, true);
        }
    }

    //initlizes the player handler
    public void Initilize(int maxHPStarting, 
        float walkSpeedStarting, float runSpeedStarting, float sprintSpeedStarting,
        bool canSprintStarting)
    {
        //Get playerHandler objects first
        playerRig = gameObject.transform.GetChild(0).transform.gameObject;
        playerAura = gameObject.transform.GetChild(1).transform.gameObject;
        anim = GetComponent<Animator>();

        //Set Player Bools
        lockMovement = false;

        //initalize all added on componenets
        if (GetComponent<HealthBase>() != null)
            GetComponent<HealthBase>().Init(debugMode);

        //set all player stat variables here
        hpMax = maxHPStarting;
        hpCurrent = hpMax;
        walkSpeed = walkSpeedStarting;
        runSpeed = runSpeedStarting;
        sprintSpeed = sprintSpeedStarting;
        canSprint = canSprintStarting;
    }

    //run a coroutine that has the player disabled until the room has been transitioned
    public void TransitionPlayerToRoomPosition(Vector3 roomPosition)
    {
        StartCoroutine(TransitionRoutine(roomPosition));
    }

    //disables the player then transitions him from one room to another
    IEnumerator TransitionRoutine(Vector3 roomPosition)
    {
        lockMovement = true;
        SetPlayerModelActive(false);
        yield return new WaitForSeconds(GameHandler.instance.roomTransitionTime);
        MovePlayerPosition(roomPosition);
        SetPlayerModelActive(true);
        lockMovement = false;
        yield return null;
    }

    //sets just the player model and the aura to be active or inactive
    public void SetPlayerModelActive(bool modifier)
    {
        playerRig.SetActive(modifier);
        playerAura.SetActive(modifier);
    }

    //moves the player armature to a specified position
    public void MovePlayerPosition(Vector3 destination)
    {
        // need to add a vector3 that adds one unit forward
        playerRig.transform.position = destination;   
    }

    //get the player model's position
    public Vector3 GetPlayerPosition()
    {
        return playerRig.transform.position;
    }

    //get the player rig for reference
    public GameObject GetPlayerRig()
    {
        return playerRig;
    }

    //give the player (or take away) the ability to move and shoot fireballs
    public void SetPlayerControl(bool modifier)
    {
        playerAura.SetActive(modifier);
        canSprint = modifier;
        lockMovement = !modifier;
    }

    //method that transfers the variables from one source to another for copy/paste methods
    void TransferVariables(PlayerHandler source, PlayerHandler destination)
    {
        destination.hpCurrent = source.hpCurrent;
        destination.hpMax = source.hpMax;
    }
    public void TakeDamage(float amount)
    {
        // Set the damaged flag so the screen will flash.
        damaged = true;

        // Reduce the current health by the damage amount.
        hpCurrent -= amount;

        // Set the health bar's value to the current health.
        //healthSlider.value = hpCurrent;
    
        // Play the hurt sound effect.
        //playerAudio.Play();

        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (hpCurrent <= 0 && !isDead)
        {
            // ... it should die.
            Death();
        }
    }

    void Death()
    {
        // Set the death flag so this function won't be called again.
        isDead = true;

        //set the player to not be able to control the model anymore
        SetPlayerControl(false);

        // Turn off any remaining shooting effects.
        //playerShooting.DisableEffects();

        // Tell the animator that the player is dead.
        //anim.SetTrigger("PlayerDead");

        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
       // playerAudio.clip = deathClip;
        //playerAudio.Play();

        // Turn off the movement and shooting scripts.
        //playerMovement.enabled = false;
        //playerShooting.enabled = false;
    }

}
