﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthHearts : MonoBehaviour
{
    HealthBase hb;
    [SerializeField] private Sprite heart0;
    [SerializeField] private Sprite heart1;

    private List<HeartImage> heartImageList;
    private Vector2 heartSlot1;
    private Vector2 heartSlot2;
    private Vector2 heartSlot3;

    void Awake()
    {
        heartImageList = new List<HeartImage>();
        heartSlot1 = new Vector2(0, 0);
        heartSlot2 = new Vector2(50, 0);
        heartSlot3 = new Vector2(100, 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        //CreateHeart(heartSlot1).SetHeartFragments(0);
        //CreateHeart(heartSlot2).SetHeartFragments(0);
        //CreateHeart(heartSlot3).SetHeartFragments(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public HeartImage CreateHeart(Vector2 anchoredPosition)
    {
        GameObject heartObject = new GameObject("Heart", typeof(Image));

        heartObject.transform.parent = transform;
        heartObject.transform.localPosition = Vector3.zero;

        heartObject.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;
        heartObject.GetComponent<RectTransform>().sizeDelta = new Vector2(50,50);

        Image heartImageUI = heartObject.GetComponent<Image>();
        heartImageUI.sprite = heart0;

        HeartImage heartImage = new HeartImage(this, heartImageUI);
        heartImageList.Add(heartImage);

        return heartImage;
    }

    public class HeartImage
    {
        private Image heartImage;
        private healthHearts healthH;

        public HeartImage(healthHearts healthH, Image heartImage)
        {
            this.healthH = healthH;
            this.heartImage = heartImage;
        }      
    

        public void SetHeartFragments(int fragments)
        {
            switch (fragments)
            {
                //Filled
                case 0: heartImage.sprite = healthH.heart0; break;
                //Half
                case 1: heartImage.type = Image.Type.Filled; heartImage.fillAmount = 0.5f; heartImage.sprite = healthH.heart0; break;
                //Empty
                case 2: heartImage.sprite = healthH.heart1; break;
            }
        }     
    }
}
