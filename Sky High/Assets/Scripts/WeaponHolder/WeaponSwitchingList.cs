﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitchingList : MonoBehaviour
{
    // To enable weapon switching, this script is to be attatched to an empty object
    // preferably named "WeaponHolder" or something similar. Spell launchers are then attached to the WeaponHolder. 
    // Order of the weapons starts with 0, with the top-most object being 0
    // And lastly, be sure the WeaponHolder is attached to a player.

    public int selectedWeapon = 0;      
    private int previousWepon;
    public string swapButton;

    private List<Transform> weapons = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        LoadWeaponList();
        constrainSelectedWeapon();
        SelectWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(swapButton))
        {
            selectedWeapon += 1; 
        }

        constrainSelectedWeapon();

        if (previousWepon != selectedWeapon)
        {
            SelectWeapon();
            weapons[previousWepon].gameObject.SetActive(false);
            previousWepon = selectedWeapon;
        }
    }

    void LoadWeaponList()
    {
        foreach (Transform weapon in transform)
        {
            weapon.gameObject.SetActive(false);
            weapons.Add(weapon);
        }
    }
    void SelectWeapon()
    {
        weapons[selectedWeapon].gameObject.SetActive(true);
    }

    void constrainSelectedWeapon()
    {
        if (selectedWeapon >= weapons.Count || selectedWeapon < 0)
        {
            selectedWeapon = 0;
        }
    }
}
