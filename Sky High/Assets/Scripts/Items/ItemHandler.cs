﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHandler : MonoBehaviour
{
    public bool debugMode = false;                      //if we are debugging the item behavior..

    RoomNode assignedRoom;                              //the room the item is assigned to
    Vector3 spawnPointPosition;                         //the place where the item spawns at

    //when the object starts at runtime check to see if the object is being debugged
    private void Start()
    {
        //if we are debugging this item... initalize with bogus variables
        if (debugMode)
        {
            Debug.Log("Debug Mode is on for gameObject: " + gameObject.name.ToString());
            Init(null, gameObject.transform.position);
        }
    }

    //Initialize the item scripts based on if we are debugging or not
    public void Init(RoomNode roomAssignedTo, Vector3 spawnPointAssignedTo)
    {
        assignedRoom = roomAssignedTo;
        spawnPointPosition = spawnPointAssignedTo;

        if (GetComponent<HealthPotion>() != null)
            GetComponent<HealthPotion>().Init(debugMode);

        if (GetComponent<DamageUpTome>() != null)
            GetComponent<DamageUpTome>().Init(debugMode);

        if (GetComponent<HealthUpHeart>() != null)
            GetComponent<HealthUpHeart>().Init(debugMode);
    }

    //when this item is destroyed, remove it from the item list
    public void OnDestroy()
    {
        if (!debugMode)
            assignedRoom.RemoveItem(gameObject);
    }

}
