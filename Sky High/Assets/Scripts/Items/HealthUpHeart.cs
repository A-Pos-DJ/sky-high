﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUpHeart : MonoBehaviour
{
    private bool initialized = false;

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerH;
    public float healthUp = 1.0f;

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerH = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerH = GameHandler.instance.GetPlayerHealthObject();
        }

        initialized = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!initialized)
            return;

        if (other.gameObject == playerRig)
        {
            if (GetComponent<AudioSource>() != null)
            {
                GetComponent<AudioSource>().Play();
                playerH.maxHealth += healthUp;
                playerH.currentHealth = playerH.maxHealth;
                playerH.healthSlider.maxValue = playerH.maxHealth;
                playerH.healthSlider.value = playerH.currentHealth;
                Destroy(gameObject);
            }
        }
    }
}
