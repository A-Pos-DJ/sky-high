﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotion : MonoBehaviour
{
    private bool initialized = false;

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerH;
    public float healAmount = 1f;

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerH = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerH = GameHandler.instance.GetPlayerHealthObject();
        }
        initialized = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!initialized)
            return;

        if (other.gameObject == playerRig)
        {
            if (playerH.maxHealth >= playerH.currentHealth + healAmount)
            {
                if (GetComponent<AudioSource>() != null)
                {
                    GetComponent<AudioSource>().Play();
                    playerH.currentHealth += healAmount;
                    playerH.healthSlider.value = playerH.currentHealth;
                    Destroy(gameObject);
                }
            }
        }
    }
}
