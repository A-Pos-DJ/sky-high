﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpTome : MonoBehaviour
{
    private bool initialized = false;

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerH;
    public float damageUp = 0.5f;

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerH = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerH = GameHandler.instance.GetPlayerHealthObject();
        }

        initialized = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!initialized)
            return;

        if (other.gameObject == playerRig)
        {
            if (gameObject.GetComponent<AudioSource>() != null)
            {
                gameObject.GetComponent<AudioSource>().Play();
                playerH.currentPDamage += damageUp;
                Destroy(gameObject);
            }     
        }
    }
}
