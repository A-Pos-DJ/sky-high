﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadLauncherScript : MonoBehaviour
{
    protected int currentElement = 1;
    public float fireRate = .8f;            //Shots per second
    public float damage = 60;               //Damage per projectile
    public float maxDamageMultiplier = 2;   //Projectiles get stronger as they travel, enter a multiplier here for the max bonus 
    public float speed = 20;                //Projectile movement speed
    public float statusChance = 50;         //Projectile status chance
    [SerializeField] private float activeTime;      //How long the projectile will stay active without hitting anything
    [SerializeField] private float lifespan;      //How long the projectile will stay in the scene before being destroyed

    [SerializeField] private GameObject projectile;     //Projectile to be fired
    [SerializeField] public Transform firepoint;        //Point where the projectile will be fired from
    public float[] spreadAngles = { 15, 5, -5, -15 };      //The angles at which the the projectiles will be fired at
                                                           //Number of projectiles spawned is equal to array length

    public AudioSource shotSFX;                           //Audio to play on fire

    private float timeBtwShots;
    private float timer;


    void OnEnable()
    {
        shotSFX.Stop();
    }
    // Start is called before the first frame update
    void Start()
    {
        timeBtwShots = 1 / fireRate;
    }

    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButtonDown("Fire2") && timer >= timeBtwShots)
        {
            FireProjectile();
            timer = 0;
        }
    }


    void FireProjectile()
    {
        shotSFX.Stop();
        shotSFX.Play();
        for (int i = 0; i < spreadAngles.Length; i++)
        {
            Quaternion spreadRotation = firepoint.rotation;
            spreadRotation *= Quaternion.Euler(0, spreadAngles[i], 0); 
            GameObject firedProjectile = Instantiate(projectile, firepoint.transform.position, spreadRotation);
            SetupProjectile(firedProjectile);
        }
    }

    void SetupProjectile(GameObject proj)
    {
        var attack = proj.GetComponent<SpreadShotScript>();
        attack.currentElement = currentElement;
        attack.maxMultiplier = maxDamageMultiplier;
        attack.Setup(damage, speed, statusChance, activeTime, lifespan);
    }
}
