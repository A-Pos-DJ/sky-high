﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PrimaryLauncherBase : MonoBehaviour
{
    public float fireRate = 3;          //Shots per second
    public float damage = 20;           //Damage per projectile
    public float speed = 30;            //Projectile movement speed
    public float statusChance = 10;     //Projectile status chance
    [SerializeField] private float activeTime;      //How long the projectile will stay active without hitting anything
    [SerializeField] private float lifespan;      //How long the projectile will stay in the scene before being destroyed

    [SerializeField] private GameObject projectile;     //Projectile to be fired
    [SerializeField] public Transform firepoint;        //Point where the projectile will be fired from
    public AudioSource shotSFX;                       //Audio to play on fire

    private bool singleShot;
    private float timeBtwShots;
    private float timer;

    private ElementSwitching elemSwitcher;
    protected int currentElement = 0;

    void OnEnable()
    {
        shotSFX.Stop();
    }

    // Start is called before the first frame update
    void Start()
    {
        timeBtwShots = 1 / fireRate;
    }

    void Awake()
    {
        elemSwitcher = GetComponentInParent<ElementSwitching>();
        if (elemSwitcher == null)
        {
            Debug.Log("Element switcher missing");
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(elemSwitcher != null)
        {
            currentElement = elemSwitcher.SelectedElement;
        }
        if (Input.GetButton("Fire1") && timer >= timeBtwShots)
        {
            FireProjectile();
            timer = 0;
        }
    }

    public virtual void FireProjectile()
    {
        shotSFX.Stop();
        shotSFX.Play();
        GameObject firedProjectile = Instantiate(projectile, firepoint.position, firepoint.rotation);
        SetupProjectile(firedProjectile);
    }

    public virtual void SetupProjectile(GameObject proj)
    {
        var attack = proj.GetComponent<ProjectileShotBase>();
        attack.currentElement = currentElement;
        attack.Setup(damage, speed, statusChance, activeTime, lifespan);

        /*
        //add the projectile to the projectile handler
        if (GameHandler.instance != null)
            GameHandler.instance.AddProjectile(proj);
        */
    }
}
