﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlacementLuancherBase : MonoBehaviour
{
    //Enemies and the floor will need their own layermasks for placement launchers to work
    
    [SerializeField] protected int currentElement;       //Shots current element, 0 = Fire, 1 = Ice, 3 = lightning
    public float fireRate = 1;          //Shots per second
    public float damage = 100;          //Damage per projectile
    public float statusChance = 100;     //Projectile status chance
    public float range = 3;             //Blast radius of the projectile
    public int maxTargets = 10;         //Max number of targets the blast can hit

    [SerializeField] private float activeTime;      //How long before the projectile detonates without hitting anything
    [SerializeField] private float prefabLifetime;   //How long beefore the prefab gets destroyed
    public LayerMask layerMaskToHit;             //Shots fired will only check objects on this layer

    [SerializeField] private GameObject projectile;       //Projectile to be fired
    [SerializeField] public Transform firepoint;         //Point where the projectile will be fired from
    public AudioSource shotSFX;                       //Audio to play on fire

    private float timeBtwShots;
    private float timer;

    [SerializeField] private LayerMask floorLayerMask;
    private Transform target;

    void OnEnable()
    {
        shotSFX.Stop();
    }
    // Start is called before the first frame update
    void Start()
    {
        timeBtwShots = 1 / fireRate;
        target = transform;
    }

    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButtonDown("Fire2") && timer >= timeBtwShots)
        {
            GetTargetPoint();
            FireProjectile();
            timer = 0;
        }
    }


    void FireProjectile()
    {
        shotSFX.Stop();
        shotSFX.Play();
        GameObject firedProjectile = Instantiate(projectile, target.position, firepoint.rotation);
        SetupProjectile(firedProjectile);
    }

    void SetupProjectile(GameObject proj)
    {
        var attack = proj.GetComponent<PlacementShotBase>();
        attack.currentElement = currentElement;
        attack.layerMaskToCheck = layerMaskToHit;
        attack.maxTargets = maxTargets;
        attack.Setup(damage, range, statusChance, activeTime, prefabLifetime);

        /*
        //add the projectile to the projectile handler
        if (GameHandler.instance != null)
            GameHandler.instance.AddProjectile(proj);
        */
    }

    public virtual void GetTargetPoint()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;

        if (Physics.Raycast(camRay, out rayHit, 100f, floorLayerMask))
        {
            target.position = rayHit.point;
        }
    }
}
