﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabContainer : MonoBehaviour
{
    public static PrefabContainer instance;                     //instance of the prefab container

    [Header("Player")]
    public GameObject player;

    [Header("Enemies")]
    public List<GameObject> enemies;

    [Header("Bosses")]
    public List<GameObject> bosses;

    [Header("Items")]
    public List<GameObject> items;

    [Header("PowerUps")]
    public List<GameObject> powerUps;

    [Header("Rooms")]
    public List<GameObject> rooms;

    [Header("Walls")]
    public List<GameObject> walls;

    [Header("Misc Prefabs")]
    public GameObject portal;


    //start the object with linking the instance
    private void Awake()
    {
        LinkInstance();
    }

    //create a singleton instance of the prefab container, do not overwrite if there is one currently
    void LinkInstance()
    {
        if (instance == null)
            instance = this;
    }
}
