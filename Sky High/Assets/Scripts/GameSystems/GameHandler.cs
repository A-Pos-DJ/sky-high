﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    //System Variables
    static public GameHandler instance;                         //a public static instance of the game handler that can be accessed by everyone
    PrefabContainer prefabs;                                    //the instance of the prefab container

    [Range(0, 100)]
    public int normalRoomItemSpawnChance = 30;                  //chance for a normal room to spawn an item

    public float roomTransitionTime = 1f;                       //how long it takes to transition from one room to another

    //player variables
    [SerializeField] int maxHPStarting = 3;                     //maximum HP of the player when the game starts

    [SerializeField] float walkSpeedStarting = 2.5f;
    [SerializeField] float runSpeedStarting = 3f;
    [SerializeField] float sprintSpeedStarting = 4f;

    [SerializeField] bool canSprintStarting = false;

    //insert other player variables
    GameObject player;
    PlayerHandler playerHandler;
    PlayerHP playerHP;

    //Camera Variables
    [SerializeField] public Camera mainCamera;                  //the main camera that the player views
    Vector3 mainCameraOffset = new Vector3(0, 12, -2.1f);
    Vector3 mainCameraEulerAngels = new Vector3(80, 0, 0);
    [SerializeField] public Camera miniMapCamera;               //the camera that is controlls movement of the minimap
    Vector3 miniMapCameraOffset = new Vector3(0, 60f, 0);
    Vector3 miniMapCameraEulerAngels = new Vector3(90, 0, 0);

    //Object variables
    [SerializeField] MapHandler mapHandler;                     //the map handler object that needs to be referenced
    //[SerializeField] ProjectileHandler projectileHandler;       //the projectile handler that needs to be referenced



    // Start is called before the first frame update
    void Start()
    {
        Init();
        Cursor.visible = true;
        //ensure this object cannot be destroyed in a new scene
        DontDestroyOnLoad(this);
    }

    //Initialize the Game Handler
    void Init()
    {
        LinkInstance();
        PlayerSetup();
        MapSetup();
        //ProjectileSetup();
        ResetPlayerPosition();
    }

    //create a singleton instance of the game handler, do not overwrite if there is one currently. Links Prefab container instance
    void LinkInstance()
    {
        if (instance == null)
            instance = this;

        prefabs = PrefabContainer.instance;
    }

    //create the map and move the cameras to the correct starting location
    void MapSetup()
    {
        //create the floor
        mapHandler.Init();

        //move the cameras to the starting location
        MoveCameraToRoom(mainCamera, mapHandler.currentRoom.transform);
        MoveCameraToRoom(miniMapCamera, mapHandler.currentRoom.transform);
    }

    /*
    //sets up the projectile handler
    void ProjectileSetup()
    {
        projectileHandler = new ProjectileHandler();
        projectileHandler.Init();
    }
    */

    //create the player 
    void PlayerSetup()
    {
        InstantiatePlayer();
    }

    //moves the camera instatnty to the designated location
    public void MoveCameraToRoom(Camera cameraBeingMoved, Transform roomTransform)
    {
        Vector3 destination = roomTransform.position;
        Vector3 eulerAngles = new Vector3(0, 0, 0);

        if (cameraBeingMoved == mainCamera)
        {
            destination += mainCameraOffset;
            eulerAngles += mainCameraEulerAngels;
        }
        else if (cameraBeingMoved == miniMapCamera)
        {
            destination += miniMapCameraOffset;
            eulerAngles += miniMapCameraEulerAngels;
        }
        else
        {
            Debug.Log("ERROR: unknown camera: " + cameraBeingMoved.ToString() + "is trying to be moved to a room");
            return;
        }

        cameraBeingMoved.transform.position = destination;
        cameraBeingMoved.transform.localEulerAngles = eulerAngles;
    }

    //transitions a camera movemment to a specified room based on the room transition time
    IEnumerator TransitionCameraToRoom(Camera cameraBeingMoved, Transform roomTransform)
    {
        Vector3 destination = roomTransform.position;
        Vector3 eulerAngles = new Vector3(0, 0, 0);
        Vector3 currentPos = cameraBeingMoved.transform.position;
        Vector3 currentEuler = cameraBeingMoved.transform.eulerAngles;

        //get the correct values based on the camera argument
        if (cameraBeingMoved == mainCamera)
        {
            destination += mainCameraOffset;
            eulerAngles += mainCameraEulerAngels;
        }
        else if (cameraBeingMoved == miniMapCamera)
        {
            destination += miniMapCameraOffset;
            eulerAngles += miniMapCameraEulerAngels;
        }
        else
        {
            Debug.Log("ERROR: unknown camera: " + cameraBeingMoved.ToString() + "is trying to transition using roomTransitionCamera");
            yield return null;
        }

        float percentageCompleted = 0f;

        while (percentageCompleted < 1)
        {
            percentageCompleted += Time.deltaTime / roomTransitionTime;
            cameraBeingMoved.transform.position = Vector3.Lerp(currentPos, destination, percentageCompleted);
            cameraBeingMoved.transform.eulerAngles = Vector3.Lerp(currentEuler, eulerAngles, percentageCompleted);
            yield return null;
        }
    }

    //transition the player from one room to another
    public void RoomTransition(WallNode wallNodeDestination)
    {
        //gather variables
        Vector3 destination = wallNodeDestination.transform.position;
        Vector3 destinationOffset = wallNodeDestination.transform.forward * 1.4f;
        Vector3 modifiedDestination = destination + destinationOffset;
        GameObject oldCurrentRoom = mapHandler.currentRoom;
        GameObject newCurrentRoom = wallNodeDestination.GetComponentInParent<RoomNode>().gameObject;

        //start the room transition coroutine
        StartCoroutine(RoomTransitionRoutine(oldCurrentRoom, newCurrentRoom, modifiedDestination));
    }

    //disables the player then transitions him from one room to another
    IEnumerator RoomTransitionRoutine(GameObject oldCurrentRoom, GameObject newCurrentRoom, Vector3 playerDestination)
    {
        //remove all currently active projectiles
        //projectileHandler.SetProjectilesToBeCleared();
        //deactivate the old room
        oldCurrentRoom.GetComponent<RoomNode>().DeactivateRoom();
        //assign the new room as the current room
        mapHandler.currentRoom = newCurrentRoom;
        //have the cameras transition over to the next room
        StartCoroutine(TransitionCameraToRoom(mainCamera, mapHandler.currentRoom.transform));
        StartCoroutine(TransitionCameraToRoom(miniMapCamera, mapHandler.currentRoom.transform));
        //transition the player to the new room position
        player.GetComponent<PlayerHandler>().TransitionPlayerToRoomPosition(playerDestination);
        //wait for the transiton to finish
        yield return new WaitForSeconds(roomTransitionTime);

        //activate the current room
        mapHandler.currentRoom.GetComponent<RoomNode>().ActivateRoom();
        yield return null;
    }

    //creates a copy of the player with the stats initilized from playerStats
    public void InstantiatePlayer()
    {
        player = Instantiate(prefabs.player, new Vector3(0, 0, 0), prefabs.player.transform.rotation);
        playerHandler = player.GetComponent<PlayerHandler>();
        playerHP = player.GetComponent<PlayerHP>();
        playerHandler.Initilize(maxHPStarting, walkSpeedStarting, runSpeedStarting, sprintSpeedStarting, canSprintStarting);
    }

    //move the player armiture though the player handler method
    public void MovePlayerPosition(Vector3 destination)
    {
        player.GetComponent<PlayerHandler>().MovePlayerPosition(destination);
    }

    //gets the player rig from the player handler
    public GameObject GetPlayerRig()
    {
        return playerHandler.GetPlayerRig();
    }

    //gets the player health and returns it
    public PlayerHP GetPlayerHealthObject()
    {
        return playerHP;
    }

    /*
    //adds the projectile to the projectile handler
    public void AddProjectile(GameObject projectileToBeAdded)
    {
        projectileHandler.AddProjectile(projectileToBeAdded);
    }

    //adds the projectile to the projectile handler
    public void RemoveProjectile(GameObject projectileToBeRemoved)
    {
        projectileHandler.RemoveProjectile(projectileToBeRemoved);
    }
    */

    //reset the player position to the begining room
    void ResetPlayerPosition()
    {
        playerHandler.MovePlayerPosition(mapHandler.currentRoom.transform.position);
    }

    //load the next floor
    public void LoadNextFloor()
    {
        int floorNum = mapHandler.floorNumber;
        int random = 3;
        if (3 - floorNum < 0)
            random = 0;
        else
            random = 3 - floorNum;

        //projectileHandler.SetProjectilesToBeCleared();
        mapHandler.DestroyCurrentFloor();
        playerHandler.SetPlayerModelActive(false);

        mapHandler.CreateNewFloor(5 + (int)(floorNum / 2 ), 5 + (int)(floorNum / 2), 
            Random.Range(random, 5 + (int)(floorNum / 2)), 4 + floorNum, normalRoomItemSpawnChance, prefabs);

        ResetPlayerPosition();
        playerHandler.SetPlayerModelActive(true);

        //move the cameras to the starting location
        MoveCameraToRoom(mainCamera, mapHandler.currentRoom.transform);
        MoveCameraToRoom(miniMapCamera, mapHandler.currentRoom.transform);

    }

}
