﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadShotScript : ProjectileShotBase
{
    public float maxMultiplier { get; set; }
    float damageBonus;
    float currentBonus;
    float timer;
    Vector3 startScale = new Vector3(.5f, .5f, .5f);
    Vector3 targetScale;



    public override void Setup(float iDamage, float iSpeed, float iStatusChance, float iActiveTime, float iPrefabLifespan)
    {
        base.Setup(iDamage, iSpeed, iStatusChance, iActiveTime, iPrefabLifespan);
        timer = 0;
        currentBonus = 0;
        damageBonus = damage * (maxMultiplier - 1);
    }

    // Update is called once per frame
    new void Update()
    {
        timer += Time.deltaTime;
        base.Update();
        currentBonus = (damageBonus * (timer/activeTime));
        //transform.localScale = Vector3.Lerp(startScale, targetScale, timer/activeTime); 
        //Vector3 newscale = transform.localScale;
        //newscale *= 1.02f;
        //transform.localScale = newscale;
        
    }

    public override void DealDamage(Collider other)
    {
        damage += currentBonus;
        base.DealDamage(other);
        Debug.Log("Dealt " + currentBonus + " Bonus damage");
    }
}
