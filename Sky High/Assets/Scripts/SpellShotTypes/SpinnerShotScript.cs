﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerShotScript : ProjectileShotBase
{
    [SerializeField] private Transform rotator;

    public override void Setup(float iDamage, float iSpeed, float iStatusChance, float iActiveTime, float iPrefabLifespan)
    {
        base.Setup(iDamage, iSpeed, iStatusChance, iActiveTime, iPrefabLifespan);
        setupSpinnerProjectiles();
    }

    void OnTriggerEnter(Collider other)
    {
        
    }

    public override void DeactivatePrefab()
    {
        gameObject.SetActive(false);
    }

    void setupSpinnerProjectiles()
    {
        foreach(Transform projectile in rotator)
        {
            SpinnerProjectileScript proj = projectile.GetComponent<SpinnerProjectileScript>();
            proj.damage = damage;
            proj.statusChance = statusChance;
            proj.currentElement = currentElement;
        }
    }
    
}
