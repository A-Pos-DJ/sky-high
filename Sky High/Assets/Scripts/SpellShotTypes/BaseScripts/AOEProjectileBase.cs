﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AOEProjectileBase : MonoBehaviour
{
    public int currentElement { get; set; }
    public float damage { get; set; }
    public float statusChance { get; set; }
    public float range { get; set; }
    public int maxTargets { get; set; }
    public LayerMask LayerMaskToCheck { get; set; }



    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    public virtual void Setup(float iDamage, float iStatusChance, float iRange, int iCurrentElement)
    {
        damage = iDamage;
        statusChance = iStatusChance;
        range = iRange;
        currentElement = iCurrentElement;

        FindTargets();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void DealDamage(Collider other)
    {
        var targetHP = other.GetComponent<HealthBase>();
        if (currentElement == 0)
        {
            targetHP.takeFireDamage(damage, statusChance);
        }
        else if (currentElement == 1)
        {
            targetHP.takeIceDamage(damage, statusChance);
        }
        else if (currentElement == 2)
        {
            targetHP.takeLightningDamage(damage, statusChance);
        }
    }

    public virtual void FindTargets()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, range, LayerMaskToCheck);
        DamageTargets(colliders);
    }

    public virtual void DamageTargets(Collider[] other)
    {
        for (int i = 0; i < other.Length; i++)
        {
            DealDamage(other[i]);
            Rigidbody rb = other[i].GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(50, transform.position, range);
            }
            if (i == maxTargets - 1)
            {
                i = other.Length; 
            }
        }
    } 
}
