﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerProjectileScript : MonoBehaviour
{
    public int currentElement { get; set; }
    public float damage { get; set; }
    public float statusChance { get; set; }
    //public GameObject hitFX;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            DealDamage(other);
            deactivatePrefab();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("SolidObject"))
        {
            deactivatePrefab();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            deactivatePrefab();
        }
    }

    void deactivatePrefab()
    {
        //hitFX.SetActive(true);
        gameObject.SetActive(false);
    }

    public void DealDamage(Collider other)
    {
        //check to see if the object can take damage
        if (!ThisCanTakeDamage(other))
            return;

        var targetHP = other.GetComponent<HealthBase>();
        if (currentElement == 0)
        {
            targetHP.takeFireDamage(damage, statusChance);
        }
        else if (currentElement == 1)
        {
            targetHP.takeIceDamage(damage, statusChance);
        }
        else if (currentElement == 2)
        {
            targetHP.takeLightningDamage(damage, statusChance);
        }
    }

    //checks to see if the object can be hit and if there is still HP left
    bool ThisCanTakeDamage(Collider collisionObject)
    {
        if (collisionObject.GetComponent<HealthBase>() == null)
            return false;

        return !collisionObject.GetComponent<HealthBase>().IsThisDyingOrDead();
    }
}
