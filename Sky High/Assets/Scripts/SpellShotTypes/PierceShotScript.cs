﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PierceShotScript : ProjectileShotBase
{
    private float trailBuffer;
    //private ParticleSystem shotParticles;

    void Start()
    {
        //shotParticles = GetComponentInChildren<ParticleSystem>();
    }

    new void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            DealDamage(other);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            DeactivatePrefab();
        }
    }

    public override void DeactivatePrefab()
    {
        isDisabled = true;
        //shotParticles.Stop();
        trailBuffer = activeTime + 2;
        Invoke("WaitForTrail", trailBuffer);
    }

    private void WaitForTrail()
    {
        gameObject.SetActive(false);
    }
}
