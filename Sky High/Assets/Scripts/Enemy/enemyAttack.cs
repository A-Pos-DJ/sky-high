﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAttack : MonoBehaviour
{
    private bool initialized = false;               //if the class' variables have been initialized

    public float timeBetweenAttacks = 1.5f;         // The time in seconds between each attack.
    public float attackDamage = 0.5f;               // The amount of health taken away per attack.

    Animator anim;                                  // Reference to the animator component.
    HealthBase enemyH;                                 // Reference to this enemy's health.
    GameObject playerRig;                           // Reference to the playerRig GameObject.
    HealthBase playerHP;                              // Reference to the player's health.
    public bool playerInRange;                      // Whether player is within the trigger collider and can be attacked.
    float timer;                                    // Timer for counting up to the next attack.

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        enemyH = GetComponent<enemyHP>();
        anim = GetComponent<Animator>();
        initialized = true;
    }

    void OnTriggerEnter(Collider other)
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // If the entering collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is in range.
            playerInRange = true;
            anim.SetBool("playerInRange", true);
        }
    }


    void OnTriggerExit(Collider other)
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // If the exiting collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is no longer in range.
            playerInRange = false;
            anim.SetBool("playerInRange", false);
        }
    }

    void Update()
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && playerInRange && enemyH.currentHealth > 0)
        {
            // ... attack.
            Attack();
        }

        // If the player has zero or less health...
        if (playerHP.currentHealth <= 0)
        {
            // ... tell the animator the player is dead.
            anim.SetBool("PlayerDead", true);
        }
    }


    public void Attack()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
        if (playerHP.currentHealth > 0)
        {
            // ... damage the player.
            playerHP.takeDamage(playerHP.currentEDamage);
        }
    }

    
}
