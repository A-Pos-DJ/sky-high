﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHP : HealthBase
{
    //base.Die()'s code just deactivates the object, you can put any code you want
    //to happen before then above it, or below for anything you might want to do after the base Die()
    public override void Die()
    {
        base.Die(); //The code that's in Die() on HealthBase
    }
}