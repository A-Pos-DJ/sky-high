﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turretAI : MonoBehaviour
{
    private bool initialized = false;           //if the class' variables have been initialized

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerHP;                          // Reference to the player's health.
    HealthBase enemyHP;                         // Reference to the enemy Hp Script
    GameObject turretHead;                      // Reference the head of the turret that will be rotating
    GameObject launcher;                        // Reference to the part of the turret that will fire out the projectile
    GameObject chargeUpRig;                     // Reference to the charging up effects attached to the turret
    public GameObject shotPrefab;               // Reference to the shot that will be fired by the turret

    public float rotationSpeed = 1.5f;          // Speed at which the turret head rotates
    public float attackDamage = 0.5f;           // The amount of health taken away per attack.
    public float chargeUpCooldown = 2f;         // the time in seconds the laser will take to charge up
    public LayerMask laserShouldPenetrate;      // Layermask reference to what the laser should penetrate

    private float chargeUpTimer = 0f;           // Timer to keep track of the amount of time that has elapsed
    private bool chargingUp = false;            // in the middle of charging up to fire the laser
    private bool chargedUp = false;             // charged up and ready to fire the laser


    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        // Set up the references.
        enemyHP = GetComponent<HealthBase>();

        turretHead = transform.GetChild(0).gameObject.
            transform.GetChild(1).gameObject.
            transform.GetChild(0).gameObject;

        chargeUpRig = turretHead.transform.GetChild(1).gameObject;
        launcher = turretHead.transform.GetChild(2).gameObject;

        chargeUpRig.SetActive(false);

        initialized = true;
    }

    private void Update()
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        // add to the charge up time when charging up
        if(chargingUp)
            chargeUpTimer += Time.deltaTime;

        //if we are not charging up the laser... keep rotating
        if(!chargingUp && !chargedUp)
            RotateHead();

        //if we are not charged up and we can see the player... start charging up
        if (!chargedUp && !chargingUp && IsPlayerInSight())
            StartChargeUp();

        //if we are charging up... continue
        if (chargingUp)
            ChargeUpLaser();

        // If the turret is charged up and this enemy is alive...
        if (chargedUp && enemyHP.currentHealth > 0)
            Attack();
    }

    //rotates the head of the turret by the rotationSpeed variable
    private void RotateHead()
    {
        turretHead.transform.Rotate(new Vector3(0, rotationSpeed, 0)); 
    }

    //checks to see if player is within shot range
    bool IsPlayerInSight()
    {
        Ray attackRay = new Ray(turretHead.transform.position, turretHead.transform.TransformDirection(Vector3.right));
        RaycastHit objectHit;

        if (Physics.Raycast(attackRay, out objectHit, Mathf.Infinity, laserShouldPenetrate))
            if (objectHit.collider.gameObject == playerRig)
                return true;

        return false;
    }

    //starts the charging up process
    void StartChargeUp()
    {
        chargeUpRig.SetActive(true);
        chargingUp = true;
    }

    //charges up the turret for a beam shot, player has the change to move away
    void ChargeUpLaser()
    {
        if (chargeUpTimer >= chargeUpCooldown)
        {
            chargeUpRig.SetActive(false);
            chargingUp = false;
            chargedUp = true;
        }
    }

    //shoots the beam, if the player is still in attacking range... he takes damage
    public void Attack()
    {
        /*
        Ray attackRay = new Ray(launcher.transform.position, turretHead.transform.TransformDirection(Vector3.right));
        if (Physics.Raycast(attackRay, Mathf.Infinity, laserShouldPenetrate))
        {
            
            //if the player is still in the way of the ray
            if (IsPlayerInSight())
            {
                // If the player has health to lose...
                if (playerHP.currentHealth > 0)
                {
                    // ... damage the player.
                    playerHP.takeDamage(attackDamage);
                }
            }
            
        }
        */

        FireProjectile();
        chargeUpRig.SetActive(false);
        chargingUp = false;
        chargedUp = false;
        chargeUpTimer = 0f;
    }

    public void FireProjectile()
    {
        Quaternion rotation = turretHead.transform.rotation;
        rotation.eulerAngles += new Vector3(0, 90, 0);
        GameObject firedProjectile = Instantiate(shotPrefab, turretHead.transform.position, rotation);
        SetupProjectile(firedProjectile);
    }

    public void SetupProjectile(GameObject proj)
    {
        var attack = proj.GetComponent<ProjectileShotBase>();
        attack.currentElement = 2;
        attack.Setup(0.5f, 50, 60, 3, 10);

        /*
        //add the projectile to the projectile handler
        if (GameHandler.instance != null)
            GameHandler.instance.AddProjectile(proj);
        */
    }
}
