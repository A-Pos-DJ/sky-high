﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skullAI : MonoBehaviour
{
    private bool initialized = false;           //if the class' variables have been initialized

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerHP;                          // Reference to the player's health.

    GameObject skullRig;
    HealthBase enemyHP;

    public float movementSpeed = 1f;
    public LayerMask whatIsWall = 0;
    public float maxDistFromWall = 1.0f;
    public float damage = 0.5f;
    float timer;
    float timeBetweenAttacks = 1.5f;

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        if (whatIsWall == 0)
            whatIsWall = LayerMask.NameToLayer("Wall");

        skullRig = gameObject.transform.GetChild(0).transform.gameObject;
        enemyHP =  gameObject.GetComponent<enemyHP>();

        ChangeRotationRandomly();
        initialized = true;
    }

    // Update is called once per frame
    void Update()
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        timer += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        //if the space in front of the skull will collide with the wall...
        if (Physics.Raycast(skullRig.transform.position, skullRig.transform.TransformDirection(Vector3.forward), maxDistFromWall, whatIsWall))
        {
            ChangeRotationRandomly();
        }
        else // otherwise, keep going the same direction
        {
            transform.Translate(Vector3.forward * (movementSpeed * 0.1f));
        }
    }


    //changes rotation randomly, will not keep going forward, must turn no matter what
    void ChangeRotationRandomly()
    {
        int randomNumber = Random.Range(1, 3);

        switch (randomNumber)
        {
            //turn right
            case 1:
                transform.Rotate(0, 90 ,0);
                break;
            //turn left
            case 2:
                transform.Rotate(0, 270, 0);
                break;
            //turn around
            case 3:
                transform.Rotate(0, 180, 0);
                break;
            default:
                Debug.Log("ERROR: Unable to change the rotation for skull: ", gameObject);
                break;
        }
        return;
    }

    void OnTriggerEnter(Collider other)
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        if (other.gameObject.tag == "Player")
        {
            //change the rotation of the skull once the player has been collided with
            ChangeRotationRandomly();

            if (timer >= timeBetweenAttacks)
            {
                // Reset the timer.
                timer = 0f;

                // If the player has health to lose...
                if (playerHP.currentHealth > 0f)
                {
                    // ... damage the player.
                    playerHP.takeDamage(playerHP.currentEDamage);
                }
            }
        }
    }
}
