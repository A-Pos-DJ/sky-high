﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EnemyHandler : MonoBehaviour
{
    public bool debugMode = false;                      //if we are debugging the enemy behavior..

    [Range(0, 100)]
    public int itemDropChance;                          //chance for this enemy to drop an item
    public List<GameObject> itemDropPrefabs;            //the list of possible items that can be dropped by this enemy

    RoomNode assignedRoom;                              //the room the enemy is assigned to
    Vector3 spawnPointPosition;                         //the place where the enemy spawns at

    //when the object starts at runtime check to see if the object is being debugged
    private void Start()
    {
        //if we are debugging this enemy... initalize with bogus variables
        if (debugMode)
        {
            Debug.Log("Debug Mode is on for gameObject: " + gameObject.name.ToString());
            Init(null, gameObject.transform.position);
        }
    }

    //Initialize the enemy scripts based on if we are debugging or not
    public void Init(RoomNode roomAssignedTo, Vector3 spawnPointAssignedTo)
    {
        assignedRoom = roomAssignedTo;
        spawnPointPosition = spawnPointAssignedTo;

        if (GetComponent<HealthBase>() != null)
            GetComponent<HealthBase>().Init(debugMode);
        if (GetComponent<enemyMovement>() != null)
            GetComponent<enemyMovement>().Init(debugMode);
        if (GetComponent<enemyAttack>() != null)
            GetComponent<enemyAttack>().Init(debugMode);
        if (GetComponent<Animation_Test>() != null)
            GetComponent<Animation_Test>().Init(debugMode);
        if (GetComponent<spiderAI>() != null)
            GetComponent<spiderAI>().Init(debugMode);
        if (GetComponent<skullAI>() != null)
            GetComponent<skullAI>().Init(debugMode);
        if (GetComponent<turretAI>() != null)
            GetComponent<turretAI>().Init(debugMode);
    }

    //resets the enemy to the spawnPoint
    void ResetPosition()
    {
        transform.position = spawnPointPosition;
        transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    //resets the enemy back to its original position and gives the enemy full health
    void ResetEnemy()
    {
        //    insert code on giving the enemy full health and reset values
        ResetPosition();
    }

    //activates the enemy in the room and resets their position
    public void ActivateEnemy()
    {
        ResetEnemy();
        gameObject.SetActive(true);
    }

    //deactivates the enemy in the room
    public void DeactivateEnemy()
    {
        gameObject.SetActive(false);
    }

    //randomly drop an item when the enemy is defeated
    void RandomItemDrop()
    {
        //make sure we have an item that can be dropped
        if (itemDropPrefabs == null || itemDropPrefabs.Count == 0)
            return;

        //see if the chance is greater than a randomly generated number
        int randomNumber = Random.Range(1, 100);
        if (randomNumber > itemDropChance)
            return;

        GameObject itemPrefab = itemDropPrefabs[Random.Range(0, itemDropPrefabs.Count)];
        assignedRoom.InstantiateDroppedItem(itemPrefab, gameObject.transform.position);
    }

    //when this enemy is destroyed, remove it from the enemy list
    public void OnDestroy()
    {
        if (!debugMode)
            assignedRoom.RemoveEnemy(gameObject);

        RandomItemDrop();
    }

}
