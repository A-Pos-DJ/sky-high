﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMovement : MonoBehaviour
{
    private bool initialized = false;       //if the class' variables have been initialized

    GameObject playerRig;                   // Reference to the playerRig GameObject.
    PlayerHP playerHP;                      // Reference to the player's health.
    HealthBase enemyHealth;                    // Reference to this enemy's health.
    UnityEngine.AI.NavMeshAgent nav;        // Reference to the nav mesh agent.


 

    public void Init(bool debugMode)
    {
        // Set up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        enemyHealth = gameObject.GetComponent<enemyHP>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        initialized = true;
    }

    void Update()
    {
        if (initialized)
        {
            // If the enemy and the player have health left...
            if (enemyHealth.currentHealth > 0 && playerHP.currentHealth > 0)
            {
                // ... set the destination of the nav mesh agent to the player.
                nav.SetDestination(playerRig.transform.position);
            }
            // Otherwise...
            else
            {
                // ... disable the nav mesh agent.
                nav.enabled = false;
            }
        }
    }
}
