﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spiderAI : MonoBehaviour
{
    private bool initialized = false;           //if the class' variables have been initialized

    GameObject playerRig;                       // Reference to the playerRig GameObject.
    HealthBase playerHP;                          // Reference to the player's health.
    Transform spider;
    HealthBase enemyHealth;
    Animator anim;
    private Rigidbody rbody;
    public float damage = 0.5f;
    public float speed = 1.0f ;
    [HideInInspector]
    public Vector3 destination;
    float timer;
    float attackTimer;
    float timerSpeed = 1.0f;
    float timeBetweenAttacks = 0.5f;
    float timeToMove = 0.5f;
    Vector3 lookPos;
    Quaternion lookAngle;


    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        // Set up the references.
        anim = GetComponent<Animator>();
        spider = GameObject.FindGameObjectWithTag("Spider").transform;
        enemyHealth = spider.GetComponent<HealthBase>();
        rbody = GetComponent<Rigidbody>();
        destination = transform.position;
        initialized = true;
    }

    // Update is called once per frame
    void Update()
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        if (enemyHealth.currentHealth > 0)
        {
            lookPos = playerRig.transform.position - transform.position;
            Quaternion lookAngle = Quaternion.LookRotation(lookPos, Vector3.up);
            spider.transform.rotation = lookAngle;
            timer += Time.deltaTime * timerSpeed;
            attackTimer += Time.deltaTime;

            if (timer >= timeToMove)
            {
                anim.SetBool("isMoving", true);
                transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * speed);

                if (Vector3.Distance(transform.position, destination) <= 0.01f)
                {
                    destination = playerRig.transform.position;
                    timer = 0.0f;
                }
            }

            if (Vector3.Distance(spider.position, destination) <= 0.25f)
            {
                anim.SetBool("isMoving", false);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //if we are not initalized yet... return
        if (!initialized)
            return;

        if (other.gameObject.tag == "Player")
        {
            if (attackTimer >= timeBetweenAttacks)
            {
                // Reset the timer.
                attackTimer = 0f;

                // If the player has health to lose...
                if (playerHP.currentHealth > 0f)
                {
                    // ... damage the player.
                    playerHP.takeDamage(playerHP.currentEDamage);
                }
            }
        }
    }
}
