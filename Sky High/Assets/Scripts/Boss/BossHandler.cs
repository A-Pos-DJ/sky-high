﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHandler : MonoBehaviour
{
    public bool debugMode = false;                      //if we are debugging the boss behavior..

    public List<GameObject> powerupDropPrefabs;         //the list of possible powerups that can be dropped by this enemy

    RoomNode assignedRoom;                              //the room the enemy is assigned to
    Vector3 spawnPointPosition;                         //the place where the enemy spawns at

    //when the object is awake check to see if the object is being debugged
    private void Start()
    {
        //if we are debugging this enemy... initalize with bogus variables
        if (debugMode)
        {
            Debug.Log("Debug Mode is on for gameObject: " + gameObject.name.ToString());
            if (GameObject.Find("/TestRoom") != null)
                assignedRoom = GameObject.Find("TestRoom").GetComponent<RoomNode>();
            else
                Debug.Log("Could not find the test room for the boss handler");
            Init(null, gameObject.transform.position);
        }
    }

    //Initialize the enemy scripts based on if we are debugging or not
    public void Init(RoomNode roomAssignedTo, Vector3 spawnPointAssignedTo)
    {
        assignedRoom = roomAssignedTo;
        spawnPointPosition = spawnPointAssignedTo;

        if (GetComponent<HealthBase>() != null)
            GetComponent<HealthBase>().Init(debugMode);
        if (GetComponent<BossFSM>() != null)
            GetComponent<BossFSM>().Init(debugMode);
    }

    //resets the enemy to the spawnPoint
    void ResetPosition()
    {
        transform.position = spawnPointPosition;
        transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    //resets the enemy back to its original position and gives the enemy full health
    void ResetBoss()
    {
        //    insert code on giving the enemy full health and reset values
        ResetPosition();
    }

    //activates the enemy in the room and resets their position
    public void ActivateBoss()
    {
        ResetBoss();
        gameObject.SetActive(true);
    }

    //deactivates the enemy in the room
    public void DeactivateBoss()
    {
        gameObject.SetActive(false);
    }

    //gets the teleport points from the room node
    public GameObject GetTeleportPoints()
    {
        if (debugMode)
            return GameObject.Find("/TestRoom/EnemySpawnPoints");

        return assignedRoom.GetEnemySpawnPoints();
    }

    //randomly drop an item when the enemy is defeated
    void RandomPowerupDrop()
    {
        //make sure we have an item that can be dropped
        if (powerupDropPrefabs == null || powerupDropPrefabs.Count == 0)
            return;

        GameObject itemPrefab = powerupDropPrefabs[Random.Range(0, powerupDropPrefabs.Count)];
        assignedRoom.InstantiateDroppedItem(itemPrefab, gameObject.transform.position);
    }

    //when this enemy is destroyed, remove it from the enemy list
    public void OnDestroy()
    {
        if (!debugMode)
            assignedRoom.RemoveEnemy(gameObject);

        RandomPowerupDrop();
    }
}
