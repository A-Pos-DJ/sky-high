﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class BossFSM : MonoBehaviour
{
    private bool initialized = false;               // if the class' variables have been initialized
    private bool debugModeOther = false;            // if we are debugging the boss (still requires a BossHandler)

    float meleeRange = 0.75f;                       // the range in which the boss is considered close enough to melee
    float meleeDamage = 0.5f;                       // the amount of damage melee attacks do
    bool playerHitByMelee = false;                  // if the player has already been hit by this melee attack so they dont repeatly take damage

    Animator anim;                                  // Reference to the animator component.
    BossHP bossHP;                                  // Reference to this enemy's health.
    GameObject playerRig;                           // Reference to the playerRig GameObject.
    HealthBase playerHP;                            // Reference to the player's health.

    public GameObject stillIllusionPrefab;          // Reference to the still illusion prefab
    public GameObject movingIllusionPrefab;         // Reference to the moving illusion prefab
    GameObject teleportPoints;                      // Reference to the teleport points from the room node

    //Nav Agent Variables
    public enum Teleport {CLOSE, FAR};              // Teleportation Distance enumerator
    UnityEngine.AI.NavMeshAgent navAgent;           // Reference to the nav mesh agent.
    Vector3 desiredNavAgentDestination;             // The desired position of the navAgent

    //FSM State variables
    public enum State {IDLE, WALKING, MELEE, TELEPORTDEFENSE, TELEPORTOFFENSE, CREATEILLUSIONS};
    Dictionary<State, bool> state;

    bool reacting = false;                          // if the FSM is reacting as an action
    bool defensiveTeleportingEnabled = false;       // if the FSM can defensively teleport as a reaction
    bool sequence2Enabled = false;                  // if the FSM is running sequence 2
    bool sequence3Enabled = false;                  // if the FSM us running sequence 3

    //FSM timers
    Dictionary<State, float> stateActiveTimers;     // a dictonary of state active timers to keep track of how long a state should be active for
    Dictionary<State, float> cooldownTimers;        // a dictonary of state cooldown timers to keep track of how long a state should remain inactive
    float evaluationTimer = 0f;                     // a timer for the purpouse of evaluating the current condition of the boss
    float evaluationTimerValue = 3f;                // how often the FSM evaulates which sequence should be used

    //state active times                            // amount of time that...
    float idleActiveTime = 0.5f;                    // --idle should be active for
    float walkingActiveTime = 1f;                   // --walking should be active for
    float meleeActiveTime = 1.25f;                  // --melee should be active for
    float teleportDefActiveTime = 0.5f;             // --teleporting defensively should be active for
    float teleportOffActiveTime = 0.5f;             // --teleporting offensively should be active for
    float createIllusionsActiveTime = 0.5f;         // --creating illusions should be active for

    //cooldown times                                // amount of time required to pass before...
    float idleCooldownTime = 0f;                    // --the boss can try to idle again
    float walkingCooldownTime = 0f;                 // --the boss can try to walk again
    float meleeCooldownTime = 1.5f;                 // --the boss can make another melee attack
    float teleportDefCooldownTime = 10f;            // --the boss can teleport defensively again
    float teleportOffCooldownTime = 10f;            // --the boss can teleport offensively again
    float createIllusionsCooldownTime = 15f;        // --the boss can create illusions again


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                         Initaization  Methods                               //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
            playerHP = GameObject.FindGameObjectWithTag("Player(Clone)").GetComponent<PlayerHP>();
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
            playerHP = GameHandler.instance.GetPlayerHealthObject();
        }

        debugModeOther = debugMode;
        bossHP = GetComponent<BossHP>();
        anim = GetComponent<Animator>();
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        teleportPoints = GetComponent<BossHandler>().GetTeleportPoints();

        InitStates();
        InitTimers(ref stateActiveTimers);
        InitTimers(ref cooldownTimers);

        initialized = true;
    }

    //setup all of the states in the FSM
    void InitStates()
    {
        state = new Dictionary<State, bool>();
        state.Add(State.IDLE, true);
        state.Add(State.WALKING, false);
        state.Add(State.MELEE, false);
        state.Add(State.TELEPORTDEFENSE, false);
        state.Add(State.TELEPORTOFFENSE, false);
        state.Add(State.CREATEILLUSIONS, false);
    }

    //setup all of the times for a passed in dictionary in the FSM
    void InitTimers(ref Dictionary<State, float> dictionary)
    {
        dictionary = new Dictionary<State, float>();
        foreach (State state in State.GetValues(typeof(State)))
        {
            dictionary.Add(state, 0f);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                            Update  Methods                                  //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    private void Update()
    {
        //if we are not yet initalized, skip
        if (!initialized)
            return;

        RunFSMSequences();
        EvaluateCurrentCondition();

        UpdateTimers(stateActiveTimers);
        UpdateTimers(cooldownTimers);
    }

    //updates all timers in the passed in dictionary
    void UpdateTimers(Dictionary<State, float> timersToUpdate)
    {
        for (int count = 0; count < timersToUpdate.Count; count++)
        {
            KeyValuePair<State, float> element = timersToUpdate.ElementAt(count);
            if(timersToUpdate[element.Key] > 0)
                timersToUpdate[element.Key] -= Time.deltaTime;
        }
    }

    //run the FSM sequences
    void RunFSMSequences()
    {
        //if we are reacting to the player's actions
        if (reacting)
        {
            ResolveReactions();
            return;
        }

        //run attack sequences with the latest one being the priority
        if (!AttackSequence3())
            if (!AttackSequence2())
                AttackSequence1();
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                     State  &  Transition  Methods                           //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //gets the state of the FSM
    public bool GetState(State stateToGet)
    {
        return state[stateToGet];
    }

    //sets the state of the FSM
    void SetState(State stateToSet, bool modifier)
    {
        //sets the state in the script and the active timer along side it
        state[stateToSet] = modifier;
        SetTimerActive(stateActiveTimers, stateToSet);

        //sets the state in the animator
        switch (stateToSet)
        {
            case State.IDLE:
                anim.SetBool("isIdle", modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", !modifier);
                anim.SetBool("isTeleportingDefense", !modifier);
                anim.SetBool("isTeleportingOffense", !modifier);
                anim.SetBool("isCreatingIllusions", !modifier);
                break;
            case State.WALKING:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", modifier);
                anim.SetBool("isMelee", !modifier);
                anim.SetBool("isTeleportingDefense", !modifier);
                anim.SetBool("isTeleportingOffense", !modifier);
                anim.SetBool("isCreatingIllusions", !modifier);
                break;
            case State.MELEE:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", modifier);
                anim.SetBool("isTeleportingDefense", !modifier);
                anim.SetBool("isTeleportingOffense", !modifier);
                anim.SetBool("isCreatingIllusions", !modifier);
                break;
            case State.TELEPORTDEFENSE:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", !modifier);
                anim.SetBool("isTeleportingDefense", modifier);
                anim.SetBool("isTeleportingOffense", !modifier);
                anim.SetBool("isCreatingIllusions", !modifier);
                break;
            case State.TELEPORTOFFENSE:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", !modifier);
                anim.SetBool("isTeleportingDefense", !modifier);
                anim.SetBool("isTeleportingOffense", modifier);
                anim.SetBool("isCreatingIllusions", !modifier);
                break;
            case State.CREATEILLUSIONS:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", !modifier);
                anim.SetBool("isTeleportingDefense", !modifier);
                anim.SetBool("isTeleportingOffense", !modifier);
                anim.SetBool("isCreatingIllusions", modifier);
                break;
            default:
                Debug.Log("Error: could not set state animation bool: " + stateToSet.ToString());
                return;
        }
    }

    //the conditions to enter a state
    public bool ShouldIEnterState(State stateTransitionTo)
    { 
        //if the boss should be dead... Idle
        if (GetComponent<BossHP>().currentHealth <= 0 && stateTransitionTo != State.IDLE)
            return false;

        if (IsTimerActive(cooldownTimers, stateTransitionTo))
            return false;

        //sets the state in the animator
        switch (stateTransitionTo)
        {
            case State.IDLE:
                return true;
            case State.WALKING:
                if (!GetState(State.MELEE) && CanITravelToNavDestination())
                    return true;
                else { return false; }
            case State.MELEE:
                if (IsPlayerInMeleeRange())
                    return true;
                else { return false; }
            case State.TELEPORTDEFENSE:
                if (defensiveTeleportingEnabled)
                    return true;
                else { return false; }
            case State.TELEPORTOFFENSE:
                if (!GetState(State.MELEE))
                    return true;
                else { return false; }
            case State.CREATEILLUSIONS:
                if (!GetState(State.MELEE))
                    return true;
                else { return false; }
            default:
                Debug.Log("Error: could not check state transition: " + stateTransitionTo.ToString());
                return false;
        }
    }

    //the conditions to leave a state
    bool ShouldIExitState(State stateTransitionFrom)
    {
        if (IsTimerActive(stateActiveTimers, stateTransitionFrom))
            return false;

        //sets the state in the animator
        switch (stateTransitionFrom)
        {
            case State.IDLE:
                return true;
            case State.WALKING:
                if (!CanITravelToNavDestination())
                    return true;
                else { return false; }
            case State.MELEE:
                if (!IsPlayerInMeleeRange())
                    return true;
                else { return false; }
            case State.TELEPORTDEFENSE:
                return true;
            case State.TELEPORTOFFENSE:
                return true;
            case State.CREATEILLUSIONS:
                return true;
            default:
                Debug.Log("Error: could not check state transition: " + stateTransitionFrom.ToString());
                return false;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                     Conditional  Checking  Methods                          //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //checks to see if the player is within melee range
    bool IsPlayerInMeleeRange()
    {
        Vector3 rayBase = new Vector3(transform.position.x, 1.5f, transform.position.z);
        Ray attackRay = new Ray(rayBase, gameObject.transform.TransformDirection(Vector3.forward));
        RaycastHit objectHit;


        //Debug.DrawRay(rayBase, gameObject.transform.TransformDirection(Vector3.forward), Color.red);

        Debug.DrawRay(rayBase, transform.forward * meleeRange, Color.red);


        if (Physics.SphereCast(attackRay, 1.1f, out objectHit, Mathf.Infinity, ~0))
            if (objectHit.collider.gameObject == playerRig && objectHit.distance <= meleeRange)
                return true;

        return false;
    }

    //checks to see if the FSM Nav Agent can travel to the desired position
    bool CanITravelToNavDestination()
    {
        //if the agent is not even on the navmesh, return false
        if (!navAgent.isOnNavMesh)
            return false;

        //see if a path can be created to the destination
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        if (navAgent.CalculatePath(desiredNavAgentDestination, path))
            return true;
        else
            return false;
    }

    //checks to see if the player will take melee damage from where he is standing
    void CheckToSeeIfMeleeHit()
    {
        //if the FSM state is melee attacking, the attack is on cooldown, the animation is currently hitting the player, and the player has not been hit
        if (GetState(State.MELEE) && stateActiveTimers[State.MELEE] < 0.75f && !playerHitByMelee && IsPlayerInMeleeRange())
        {
            // If the player has health to lose...
            if (playerHP.currentHealth > 0)
            {
                // ... damage the player.
                playerHP.takeDamage(meleeDamage);
                playerHitByMelee = true;
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                          FSM  Action  Methods                               //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //The FSM evaluates its current standing in health and adjusts to counter the player
    void EvaluateCurrentCondition()
    {
        //if it is time to evaluate
        if (evaluationTimer <= 0)
        {
            float healthPercent = bossHP.GetHealthPercentage();
            float damageOverTime = bossHP.GetDamageOverTime();

            if (healthPercent > 80 && healthPercent <= 100)               //between 80 to 100 percent health
            {
                if (damageOverTime > 2.5f && damageOverTime <= 5)    //recieving moderate damage
                {
                    defensiveTeleportingEnabled = true;
                }
                else if (damageOverTime > 5)                         //recieving critical damage
                {
                    defensiveTeleportingEnabled = true;
                    sequence2Enabled = true;
                }
            }
            else if (healthPercent > 60 && healthPercent <= 80)           //between 60 to 80 percent health
            {
                defensiveTeleportingEnabled = true;

                if (damageOverTime > 2.5f && damageOverTime <= 5)    //recieving moderate damage
                {
                    sequence2Enabled = true;
                }
                else if (damageOverTime > 5)                         //recieving critical damage
                {
                    sequence2Enabled = true;
                    cooldownTimers[State.TELEPORTDEFENSE] = 0f;
                }
            }
            else if (healthPercent > 40 && healthPercent <= 60)           //between 40 to 60 percent health
            {
                defensiveTeleportingEnabled = true;
                sequence2Enabled = true;

                if (damageOverTime > 2.5f && damageOverTime <= 5)    //recieving moderate damage
                {
                    sequence3Enabled = true;
                }
                else if (damageOverTime > 5)                         //recieving critical damage
                {
                    sequence3Enabled = true;
                    cooldownTimers[State.TELEPORTDEFENSE] = 0f;
                    cooldownTimers[State.TELEPORTOFFENSE] = 0f;
                }

            }
            else if (healthPercent > 20 && healthPercent <= 40)           //between 20 to 40 percent health
            {
                defensiveTeleportingEnabled = true;
                sequence2Enabled = true;
                sequence3Enabled = true;

                if (damageOverTime > 2.5f && damageOverTime <= 5)    //recieving moderate damage
                {
                    teleportDefCooldownTime = 8f;
                    teleportOffActiveTime = 5f;
                    createIllusionsActiveTime = 10f;
                }
                else if (damageOverTime > 5)                         //recieving critical damage
                {
                    teleportDefCooldownTime = 8f;
                    teleportOffCooldownTime = 5f;
                    createIllusionsCooldownTime = 10f;
                    cooldownTimers[State.TELEPORTDEFENSE] = 0f;
                    cooldownTimers[State.TELEPORTOFFENSE] = 0f;
                    cooldownTimers[State.CREATEILLUSIONS] = 0f;
                }
            }
            else if (healthPercent > 0 && healthPercent <= 20)            //between 0 to 20 percent health
            {
                defensiveTeleportingEnabled = true;
                sequence2Enabled = true;
                sequence3Enabled = true;
                teleportDefCooldownTime = 8f;
                teleportOffCooldownTime = 5f;
                createIllusionsCooldownTime = 10f;

                if (damageOverTime > 2.5f && damageOverTime <= 5)    //recieving moderate damage
                {
                    cooldownTimers[State.TELEPORTDEFENSE] = 0f;
                }
                else if (damageOverTime > 5)                         //recieving critical damage
                {
                    cooldownTimers[State.TELEPORTDEFENSE] = 0f;
                    cooldownTimers[State.TELEPORTOFFENSE] = 0f;
                    cooldownTimers[State.CREATEILLUSIONS] = 0f;
                }

            }

            //reset the evaluation timer
            evaluationTimer = evaluationTimerValue;
        }
        
        //update the evaluation timer
        evaluationTimer -= Time.deltaTime;
    }

    //preforms attack sequence 1, walks to the player and attacks them, returns false if the sequence fails
    bool AttackSequence1()
    {
        if (!TryToMelee())
            if (!TryToWalkToPoint(playerRig.transform.position))
                if (!TryToIdle())
                    return false;

        return true;
    }

    //preforms attack sequence 2, teleports to the player to attack, returns false if the sequence fails
    bool AttackSequence2()
    {
        if (!sequence2Enabled)
            return false;

        if (!TryToMelee())
            if (!TryToTeleportCloseToPlayer())
                if (!TryToWalkToPoint(playerRig.transform.position))
                    return false;

        return true;
    }

    //preforms attack sequence 3, creates illusions to fool the player returns false if the sequence fails
    bool AttackSequence3()
    {
        if (!sequence3Enabled)
            return false;

        if (!TryToCreateIllusions())
            if (!TryToMelee())
                if (!TryToTeleportCloseToPlayer())
                    if (!TryToWalkToPoint(playerRig.transform.position))
                        return false;

        return true;
    }

    //resolve all pending reactions
    void ResolveReactions()
    {
        if (GetState(State.TELEPORTDEFENSE))
        {
            //check to see if we can exit the teleporting state
            if (ShouldIExitState(State.TELEPORTDEFENSE))
            {
                SetState(State.TELEPORTDEFENSE, false);
                SetTimerActive(cooldownTimers, State.TELEPORTDEFENSE);
                reacting = false;
            }
        }
    }

    //asks the FSM script if teleporting is an option as a reaction
    void ReactToTeleport()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.TELEPORTDEFENSE) && !ShouldIEnterState(State.TELEPORTDEFENSE))
            return;

        //check to see if the FSM should change to the state
        if (!GetState(State.TELEPORTDEFENSE) && ShouldIEnterState(State.TELEPORTDEFENSE))
        {
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
            SetState(State.IDLE, false);
            SetState(State.WALKING, false);
            SetState(State.MELEE, false);
            SetState(State.TELEPORTOFFENSE, false);
            SetState(State.CREATEILLUSIONS, false);

            CreateStillIllusion();
            TeleportInReferenceTo(playerRig.transform.position, Teleport.FAR);
            SetState(State.TELEPORTDEFENSE, true);
            reacting = true;
            return;
        }
    }

    //asks the FSM script if creating illusions are an option, and attempts it
    bool TryToCreateIllusions()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.CREATEILLUSIONS) && !ShouldIEnterState(State.CREATEILLUSIONS))
            return false;

        // check to see if the FSM should switch to state
        if (!GetState(State.CREATEILLUSIONS) && ShouldIEnterState(State.CREATEILLUSIONS))
        {
            SetState(State.IDLE, false);
            SetState(State.WALKING, false);
            SetState(State.MELEE, false);
            SetState(State.TELEPORTOFFENSE, false);
            SetState(State.TELEPORTDEFENSE, false);

            SpawnWithMovingIllusions();
            SetState(State.CREATEILLUSIONS, true);
            SetTimerActive(stateActiveTimers, State.CREATEILLUSIONS);
            return true;
        }
        // check to see if the FSM should switch states
        else if (GetState(State.CREATEILLUSIONS) && ShouldIExitState(State.CREATEILLUSIONS))
        {
            SetState(State.CREATEILLUSIONS, false);
            SetTimerActive(cooldownTimers, State.CREATEILLUSIONS);
            return false;
        }
        else  //continue this action
        {
            return true;
        }
    }

    //asks the FSM script if offensive teleporting is an option, and attempts it
    bool TryToTeleportCloseToPlayer()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.TELEPORTOFFENSE) && !ShouldIEnterState(State.TELEPORTOFFENSE))
            return false;

        // check to see if the FSM should switch to state
        if (!GetState(State.TELEPORTOFFENSE) && ShouldIEnterState(State.TELEPORTOFFENSE))
        {
            SetState(State.IDLE, false);
            SetState(State.WALKING, false);
            SetState(State.MELEE, false);
            SetState(State.TELEPORTDEFENSE, false);
            SetState(State.CREATEILLUSIONS, false);

            CreateStillIllusion();
            TeleportInReferenceTo(playerRig.transform.position, Teleport.CLOSE);
            SetState(State.TELEPORTOFFENSE, true);
            SetTimerActive(stateActiveTimers, State.TELEPORTOFFENSE);
            return true;
        }
        // check to see if the FSM should switch states
        else if (GetState(State.TELEPORTOFFENSE) && ShouldIExitState(State.TELEPORTOFFENSE))
        {
            SetState(State.TELEPORTOFFENSE, false);
            SetTimerActive(cooldownTimers, State.TELEPORTOFFENSE);
            return false;
        }
        else  //continue this action
        {
            return true;
        }
    }

    //asks the FSM script if melee is an option, and attempts it
    bool TryToMelee()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.MELEE) && !ShouldIEnterState(State.MELEE))
            return false;

        //check to see if the FSM should change to the melee state
        if (!GetState(State.MELEE) && ShouldIEnterState(State.MELEE))
        {
            SetState(State.IDLE, false);
            SetState(State.WALKING, false);
            SetState(State.TELEPORTOFFENSE, false);
            SetState(State.TELEPORTDEFENSE, false);
            SetState(State.CREATEILLUSIONS, false);

            SetState(State.MELEE, true);
            SetTimerActive(stateActiveTimers, State.MELEE);
            playerHitByMelee = false;
            return true;
        }
        //check to see if the FSM should exit the state
        else if (GetState(State.MELEE) && ShouldIExitState(State.MELEE))
        {
            SetState(State.MELEE, false);
            SetTimerActive(cooldownTimers, State.MELEE);
            return false;
        }
        //otherwise continue with the melee attempt
        else
        {
            //check to see if a current melee attack has hit the player
            CheckToSeeIfMeleeHit();
            return true;
        }
    }

    //asks the FSM script if walking to the point is an option, attempts it
    bool TryToWalkToPoint(Vector3 destination)
    {
        desiredNavAgentDestination = destination;

        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.WALKING) && !ShouldIEnterState(State.WALKING))
            return false;

        // check to see if the FSM should switch to the walking state
        if (ShouldIEnterState(State.WALKING))
        {
            SetState(State.IDLE, false);
            SetState(State.MELEE, false);
            SetState(State.TELEPORTOFFENSE, false);
            SetState(State.TELEPORTDEFENSE, false);
            SetState(State.CREATEILLUSIONS, false);

            navAgent.isStopped = false;
            navAgent.SetDestination(destination);
            SetState(State.WALKING, true);
            SetTimerActive(stateActiveTimers, State.WALKING);
            return true;
        }
        // check to see if the FSM should switch states
        else if (GetState(State.WALKING) && ShouldIExitState(State.WALKING))
        {
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
            SetState(State.WALKING, false);
            return false;
        }
        else  //continue this action
        {
            return true;
        }
    }

    //asks the FSM script if idling is an option, and attempts it
    bool TryToIdle()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.IDLE) && !ShouldIEnterState(State.IDLE))
            return false;

        // check to see if the FSM should switch to the idle state
        if (ShouldIEnterState(State.IDLE))
        {
            SetState(State.WALKING, false);
            SetState(State.MELEE, false);
            SetState(State.TELEPORTOFFENSE, false);
            SetState(State.TELEPORTDEFENSE, false);
            SetState(State.CREATEILLUSIONS, false);

            SetState(State.IDLE, true);
            SetTimerActive(stateActiveTimers, State.IDLE);
            return true;
        }
        // check to see if the FSM should switch states
        else if (GetState(State.IDLE) && ShouldIExitState(State.IDLE))
        {
            SetState(State.IDLE, false);
            return false;
        }
        else  //continue this action
        {
            return true;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                         Timer  Related  Methods                             //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //check to see if a timer is still active in the passed in dictonary
    bool IsTimerActive(Dictionary<State, float> timerDictionary, State timerType)
    {
        return timerDictionary[timerType] > 0;
    }

    //reset a specific timer from the passed in dictionary to make the timer active
    void SetTimerActive(Dictionary<State, float> timerDictionary, State timerType)
    {
        switch (timerType)
        {
            case State.IDLE:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = idleActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = idleCooldownTime;
                return;
            case State.WALKING:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = walkingActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = walkingCooldownTime;
                return;
            case State.MELEE:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = meleeActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = meleeCooldownTime;
                return;
            case State.TELEPORTDEFENSE:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = teleportDefActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = teleportDefCooldownTime;
                return;
            case State.TELEPORTOFFENSE:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = teleportOffActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = teleportOffCooldownTime;
                return;
            case State.CREATEILLUSIONS:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = createIllusionsActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = createIllusionsCooldownTime;
                return;
            default:
                Debug.Log("Error: could not set the " + timerDictionary.ToString() + " timer for type: " + timerType.ToString());
                return;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                              Misc  Methods                                  //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //create a still image of the boss
    void CreateStillIllusion()
    {
        GameObject stillImage = Instantiate(stillIllusionPrefab, transform.position, transform.rotation);
        stillImage.GetComponent<BossStillIllusion>().Init();
    }

    //create a moving illusion of the boss
    void CreateMovingIllusion(Vector3 illusionDestination)
    {
        //find a posiiton close to ground that the illusion could spawn at
        Vector3 positionCloseToGround = new Vector3(illusionDestination.x, 0, illusionDestination.z);

        GameObject movingImage = Instantiate(movingIllusionPrefab, positionCloseToGround, transform.rotation);
        movingImage.transform.LookAt(playerRig.transform);
        
        movingImage.GetComponent<BossMovingIllusion>().Init(debugModeOther);
    }

    //create moving illusions and have the boss mix in with them
    void SpawnWithMovingIllusions()
    {
        //a random teleport point to spawn at
        int teleportIndex = Random.Range(0, teleportPoints.transform.childCount);

        //loop though what is left in the spawn point list
        for (int index = 0; index < teleportPoints.transform.childCount; index++)
        {
            if (index != teleportIndex)
            {
                CreateMovingIllusion(teleportPoints.transform.GetChild(index).transform.position);
            }
        }

        //get the position from the teleport index and spawn closer to the ground, then lookat the player
        Vector3 teleportPosition = teleportPoints.transform.GetChild(teleportIndex).transform.position;
        Vector3 positionCloseToGround = new Vector3(teleportPosition.x, 0, teleportPosition.z);
        transform.position = positionCloseToGround;
        transform.LookAt(playerRig.transform);
    }

    //teleports the boss as far from or as close to the referenced position
    void TeleportInReferenceTo(Vector3 referencePosition, Teleport referenceEnum)
    {
        Vector3 minMaxPosition = teleportPoints.transform.GetChild(0).transform.position;
        float minMaxDistance = Vector3.Distance(referencePosition, teleportPoints.transform.GetChild(0).transform.position);

        //loop though the spawn point list and find the closest/furthest point
        for (int index = 0; index < teleportPoints.transform.childCount; index++)
        {
            Vector3 teleportLocation = teleportPoints.transform.GetChild(index).transform.position;

            if (referenceEnum == Teleport.CLOSE)
            {
                if (minMaxDistance > Vector3.Distance(referencePosition, teleportPoints.transform.GetChild(index).transform.position))
                {
                    minMaxPosition = teleportPoints.transform.GetChild(index).transform.position;
                    minMaxDistance = Vector3.Distance(referencePosition, teleportPoints.transform.GetChild(index).transform.position);
                }
            }
            else if (referenceEnum == Teleport.FAR)
            {
                if (minMaxDistance < Vector3.Distance(referencePosition, teleportPoints.transform.GetChild(index).transform.position))
                {
                    minMaxPosition = teleportPoints.transform.GetChild(index).transform.position;
                    minMaxDistance = Vector3.Distance(referencePosition, teleportPoints.transform.GetChild(index).transform.position);
                }
            }
        }

        //move the boss to the specified point looking a the object they were trying to teleport from/to
        Vector3 positionCloseToGround = new Vector3(minMaxPosition.x, 0, minMaxPosition.z);
        transform.position = positionCloseToGround;
        transform.LookAt(referencePosition);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fireball")
        {
            ReactToTeleport();
        }
    }

}
