﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class BossMovingIllusion : MonoBehaviour
{
    private bool initialized = false;               // if the class' variables have been initialized

    float timeBeforeDestruction = 7.5f;             // the amount of time that passes before the illusion gets destroyed automatically
    float destructionTimer = 0f;                    // the timer that keeps track of when the game object is supposed to be destroyed
    float meleeRange = 0.65f;                       // the range in which the illusion is considered close enough to melee

    Animator anim;                                  // Reference to the animator component.
    GameObject playerRig;                           // Reference to the playerRig GameObject.

    //Nav Agent Variables
    UnityEngine.AI.NavMeshAgent navAgent;           // Reference to the nav mesh agent.
    Vector3 desiredNavAgentDestination;             // The desired position of the navAgent

    //FSM State variables
    public enum State {IDLE, WALKING, MELEE};
    Dictionary<State, bool> state;

    //FSM timers
    Dictionary<State, float> stateActiveTimers;     // a dictonary of state active timers to keep track of how long a state should be active for
    Dictionary<State, float> cooldownTimers;        // a dictonary of state cooldown timers to keep track of how long a state should remain inactive

    //state active times                            // amount of time that...
    float idleActiveTime = 0.5f;                    // --idle should be active for
    float walkingActiveTime = 1f;                   // --walking should be active for
    float meleeActiveTime = 1.25f;                  // --melee should be active for

    //cooldown times                                // amount of time required to pass before...
    public float idleCooldownTime = 0f;             // --the illusion can try to idle again
    public float walkingCooldownTime = 0f;          // --the illusion can try to walk again
    public float meleeCooldownTime = 1.5f;          // --the illusion can make another melee attack


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                         Initaization  Methods                               //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        // Setting up the references.
        if (debugMode)
        {
            playerRig = GameObject.FindGameObjectWithTag("Player(Clone)").transform.GetChild(0).gameObject;
        }
        else if (!debugMode)
        {
            playerRig = GameHandler.instance.GetPlayerRig();
        }

        anim = GetComponent<Animator>();
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        InitStates();
        InitTimers(ref stateActiveTimers);
        InitTimers(ref cooldownTimers);
        destructionTimer = timeBeforeDestruction;

        initialized = true;
    }

    //setup all of the states in the FSM
    void InitStates()
    {
        state = new Dictionary<State, bool>();
        state.Add(State.IDLE, true);
        state.Add(State.WALKING, false);
        state.Add(State.MELEE, false);
    }

    //setup all of the times for a passed in dictionary in the FSM
    void InitTimers(ref Dictionary<State, float> dictionary)
    {
        dictionary = new Dictionary<State, float>();
        foreach (State state in State.GetValues(typeof(State)))
        {
            dictionary.Add(state, 0f);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                            Update  Methods                                  //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    private void Update()
    {
        //if we are not yet initalized, skip
        if (!initialized)
            return;

        RunFSMSequences();

        UpdateTimers(stateActiveTimers);
        UpdateTimers(cooldownTimers);
        destructionTimer -= Time.deltaTime;
        if (destructionTimer <= 0)
            Destroy(this);
    }

    //updates all timers in the passed in dictionary
    void UpdateTimers(Dictionary<State, float> timersToUpdate)
    {
        for (int count = 0; count < timersToUpdate.Count; count++)
        {
            KeyValuePair<State, float> element = timersToUpdate.ElementAt(count);
            if(timersToUpdate[element.Key] > 0)
                timersToUpdate[element.Key] -= Time.deltaTime;
        }
    }

    //run the FSM sequences
    void RunFSMSequences()
    {
            AttackSequence1();
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                     State  &  Transition  Methods                           //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //gets the state of the FSM
    public bool GetState(State stateToGet)
    {
        return state[stateToGet];
    }

    //sets the state of the FSM
    void SetState(State stateToSet, bool modifier)
    {
        //sets the state in the script and the active timer along side it
        state[stateToSet] = modifier;
        SetTimerActive(stateActiveTimers, stateToSet);

        //sets the state in the animator
        switch (stateToSet)
        {
            case State.IDLE:
                anim.SetBool("isIdle", modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", !modifier);
                break;
            case State.WALKING:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", modifier);
                anim.SetBool("isMelee", !modifier);
                break;
            case State.MELEE:
                anim.SetBool("isIdle", !modifier);
                anim.SetBool("isWalking", !modifier);
                anim.SetBool("isMelee", modifier);
                break;
            default:
                Debug.Log("Error: could not set state animation bool: " + stateToSet.ToString());
                return;
        }
    }

    //the conditions to enter a state
    public bool ShouldIEnterState(State stateTransitionTo)
    {
        if (IsTimerActive(cooldownTimers, stateTransitionTo))
            return false;

        //sets the state in the animator
        switch (stateTransitionTo)
        {
            case State.IDLE:
                return true;
            case State.WALKING:
                if (!GetState(State.MELEE) && CanITravelToNavDestination())
                    return true;
                else { return false; }
            case State.MELEE:
                if (IsPlayerInMeleeRange())
                    return true;
                else { return false; }
            default:
                Debug.Log("Error: could not check state transition: " + stateTransitionTo.ToString());
                return false;
        }
    }

    //the conditions to leave a state
    bool ShouldIExitState(State stateTransitionFrom)
    {
        if (IsTimerActive(stateActiveTimers, stateTransitionFrom))
            return false;

        //sets the state in the animator
        switch (stateTransitionFrom)
        {
            case State.IDLE:
                return true;
            case State.WALKING:
                if (!CanITravelToNavDestination())
                    return true;
                else { return false; }
            case State.MELEE:
                if (!IsPlayerInMeleeRange())
                    return true;
                else { return false; }
            default:
                Debug.Log("Error: could not check state transition: " + stateTransitionFrom.ToString());
                return false;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                     Conditional  Checking  Methods                          //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //checks to see if the player is within melee range
    bool IsPlayerInMeleeRange()
    {
        Vector3 rayBase = new Vector3(transform.position.x, 1.5f, transform.position.z);
        Ray attackRay = new Ray(rayBase, gameObject.transform.TransformDirection(Vector3.forward));
        RaycastHit objectHit;


        //Debug.DrawRay(rayBase, gameObject.transform.TransformDirection(Vector3.forward), Color.red);

        Debug.DrawRay(rayBase, transform.forward * meleeRange, Color.red);


        if (Physics.SphereCast(attackRay, 1.1f, out objectHit, Mathf.Infinity, ~0))
            if (objectHit.collider.gameObject == playerRig && objectHit.distance <= meleeRange)
                return true;

        return false;
    }

    //checks to see if the FSM Nav Agent can travel to the desired position
    bool CanITravelToNavDestination()
    {
        //if the agent is not even on the navmesh, return false
        if (!navAgent.isOnNavMesh)
            return false;

        //see if a path can be created to the destination
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        if (navAgent.CalculatePath(desiredNavAgentDestination, path))
            return true;
        else
            return false;
    }

    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                          FSM  Action  Methods                               //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //preforms attack sequence 1, walks to the player and attacks them, returns false if the sequence fails
    bool AttackSequence1()
    {
        if (!TryToMelee())
            if (!TryToWalkToPoint(playerRig.transform.position))
                if (!TryToIdle())
                    return false;

        return true;
    }

    //asks the FSM script if melee is an option, and attempts it
    bool TryToMelee()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.MELEE) && !ShouldIEnterState(State.MELEE))
            return false;

        //check to see if the FSM should change to the melee state
        if (!GetState(State.MELEE) && ShouldIEnterState(State.MELEE))
        {
            SetState(State.MELEE, true);
            SetTimerActive(stateActiveTimers, State.MELEE);
            return true;
        }
        //check to see if the FSM should exit the state
        else if (GetState(State.MELEE) && ShouldIExitState(State.MELEE))
        {
            SetState(State.MELEE, false);
            SetTimerActive(cooldownTimers, State.MELEE);
            return false;
        }
        //otherwise continue with the melee attempt
        else
        {
            return true;
        }
    }

    //asks the FSM script if walking to the point is an option, attempts it
    bool TryToWalkToPoint(Vector3 destination)
    {
        desiredNavAgentDestination = destination;

        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.WALKING) && !ShouldIEnterState(State.WALKING))
            return false;

        // check to see if the FSM should switch to the walking state
        if (ShouldIEnterState(State.WALKING))
        {
            //if we can create a path, go to the destination
            navAgent.isStopped = false;
            navAgent.SetDestination(destination);
            SetState(State.WALKING, true);
            SetTimerActive(stateActiveTimers, State.WALKING);
            return true;
        }
        // check to see if the FSM should switch states
        else if (GetState(State.WALKING) && ShouldIExitState(State.WALKING))
        {
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
            SetState(State.WALKING, false);
            return false;
        }
        else  //continue this action
        {
            return true;
        }
    }

    //asks the FSM script if idling is an option, and attempts it
    bool TryToIdle()
    {
        //checks to see if the state is inactive and if it is even possible to make active
        if (!GetState(State.IDLE) && !ShouldIEnterState(State.IDLE))
            return false;

        // check to see if the FSM should switch to the idle state
        if (ShouldIEnterState(State.IDLE))
        {
            SetState(State.IDLE, true);
            SetTimerActive(stateActiveTimers, State.IDLE);
            return true;
        }
        // check to see if the FSM should switch states
        else if (GetState(State.IDLE) && ShouldIExitState(State.IDLE))
        {
            SetState(State.IDLE, false);
            return false;
        }
        else  //continue this action
        {
            return true;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                         Timer  Related  Methods                             //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //check to see if a timer is still active in the passed in dictonary
    bool IsTimerActive(Dictionary<State, float> timerDictionary, State timerType)
    {
        return timerDictionary[timerType] > 0;
    }

    //reset a specific timer from the passed in dictionary to make the timer active
    void SetTimerActive(Dictionary<State, float> timerDictionary, State timerType)
    {
        switch (timerType)
        {
            case State.IDLE:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = idleActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = idleCooldownTime;
                return;
            case State.WALKING:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = walkingActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = walkingCooldownTime;
                return;
            case State.MELEE:
                if (timerDictionary == stateActiveTimers) //state timer
                    timerDictionary[timerType] = meleeActiveTime;
                else if (timerDictionary == cooldownTimers)//cooldown timer
                    timerDictionary[timerType] = meleeCooldownTime;
                return;
            default:
                Debug.Log("Error: could not set the " + timerDictionary.ToString() + " timer for type: " + timerType.ToString());
                return;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////
    //                                                                             //
    //                              Misc  Methods                                  //
    //                                                                             //
    /////////////////////////////////////////////////////////////////////////////////

    //if the illusion gets hit with a projectile... simply destory it
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fireball")
        {
            Destroy(gameObject);
        }
    }

}
