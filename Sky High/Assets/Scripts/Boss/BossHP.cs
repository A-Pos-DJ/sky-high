﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHP : HealthBase
{
    HealthBase playerHP;

    float damageTakenTimer = 0f;
    float invulnerabilityTimer = 0f;
    float invulnerabilityTimeActive = 1f;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fireball")
        {
            if (GetComponent<BossFSM>() != null)
                if (GetComponent<BossFSM>().ShouldIEnterState(BossFSM.State.TELEPORTDEFENSE))
                {
                    invulnerabilityTimer = invulnerabilityTimeActive;
                }

            if (invulnerabilityTimer <= 0)
            {
                takeDamage(currentPDamage);
            }
        }
    }

    private void Update()
    {
        if (!initalized)
            return;

        if (invulnerabilityTimer > 0)
            invulnerabilityTimer -= Time.deltaTime;

        if(currentHealth < maxHealth)
            damageTakenTimer += Time.deltaTime;
    }

    //gets the Boss' current health percentage
    public float GetHealthPercentage()
    {
        float currentHealthPercent = currentHealth / maxHealth;
        return (float)System.Math.Round(currentHealthPercent, 2) * 100;
    }

    //gets the amount of damage taken since the FSM first took damage
    public float GetDamageOverTime()
    {
        //if we stil have max health, return undamaged
        if (currentHealth == maxHealth)
            return 0f;

        //ensure that we dont return a super high number in the first second
        if (damageTakenTimer < 1f)
            return 1f;

        float damageTakenOverTime = (maxHealth - currentHealth) / damageTakenTimer;
        return (float)System.Math.Round(damageTakenOverTime, 2);
    }


    //base.Die()'s code just deactivates the object, you can put any code you want
    //to happen before then above it, or below for anything you might want to do after the base Die()
    public override void Die()
    {
        base.Die(); //The code that's in Die() on HealthBase
    }
}