﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStillIllusion : MonoBehaviour
{
    private bool initalized = false;                        //initization variable

    public float secondsBeforeDestruction = 2.5f;           //time that is passed before the fake disappears

    private float timer;                                    //timer to keep track of time

    public void Init()
    {
        //transform.FindChild(boneName);

        /*
        for (int i = 0; i < transform.childCount; i++)
        {
            Debug.Log(bossBoneObjects[i]);
            //transform.FindChild(bossBoneObjects[i].name).position = bossBoneObjects[i].position;

        }
       
        
        for (int i = 0; i < transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).position = new Vector3(0, 0, 0);
        }
        */
        initalized = true;
    }

    private void Update()
    {
        if (!initalized)
            return;

        if (timer >= secondsBeforeDestruction)
            Destroy(gameObject);

        timer += Time.deltaTime;
    }

}
