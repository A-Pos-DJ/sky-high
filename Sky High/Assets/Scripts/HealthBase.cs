﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class HealthBase : MonoBehaviour
{
    protected bool initalized = false;

    Animator anim;
    Animation_Test dieAnim;
    public Slider healthSlider;
    public GameObject hSlider;
    public float maxHealth;
    //[HideInInspector]
    public float currentPDamage;
    public float currentEDamage = 0.5f;
    //[HideInInspector]
    public float currentHealth;
    public float FlashTime = .2f;

    //Resistances are percent based, 100 = a 100% reduction, -100 = 100% extra damage
    public float fireResistance = 0;
    public float iceResistance = 0;
    public float lightningResistance = 0;

    //If these are true, the target can receive that status effect 
    public bool canProcBurn;
    public bool canProcFreeze;
    public bool canProcShock;
    public float sinkSpeed = 2.5f;

    //[SerializeField]
    private float burnTickDamage = 5;
    //[SerializeField]
    private int burnTicks = 5;
    //[SerializeField]
    private float timeBtwBurnTicks = .5f;

    //[SerializeField]
    private float freezeTickDamage = 4;
    //[SerializeField]
    private int freezeTicks = 4;
    //[SerializeField]
    private float timeBtwFreezeTicks = 1f;

    //[SerializeField] 
    private float shockTickDamage = 30;
    //[SerializeField] 
    private int shockTicks = 1;
    //[SerializeField] 
    private float timeBtwShockTicks = 1;
    //[SerializeField] 
    private float shockCooldown = 5;
    private float shockTimer;


    [SerializeField] public GameObject body; //Holds the gameObject with the the current characetr/item's mesh renderer 
    private SkinnedMeshRenderer characterMesh;
    private Color originalColor;
    private Color flashColor = Color.red;
    private bool isDead;
    private bool isSinking;


    //initalize all references durring runtime
    public void Init(bool debugMode)
    {
        characterMesh = body.GetComponent<SkinnedMeshRenderer>();

        if (characterMesh.material.HasProperty("_Color"))
            originalColor = characterMesh.material.color;

        if (gameObject.tag != "StoneMonster")
        {
            anim = GetComponent<Animator>();
        }

        hSlider = GameObject.FindGameObjectWithTag("HealthSlider");
        healthSlider = hSlider.GetComponent<Slider>();

        if (gameObject.tag == "Player(Clone)")
        {
            healthSlider.value = maxHealth;
        }
        currentHealth = maxHealth;
        initalized = true;
    }

    void OnEnable()
    {
        //isDead = false;
        //isSinking = false;
        //currentHealth = maxHealth;

        if (gameObject.tag == "Player(Clone)")
        {
            healthSlider.value = maxHealth;
        }
        shockTimer = 0;
    }

    void Update()
    {
        if (!initalized)
            return;

        // If the enemy should be sinking...
        if (isSinking)
        {
            // ... move the enemy down by the sinkSpeed per second.
            transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }
        shockTimer -= Time.deltaTime;

        if (isDead){ return; }

        Debug.Log("Update" + currentPDamage);
    }

    public virtual void takeDamage(float damage)
    {       
        if (gameObject.tag == "Player(Clone)")
        {
            Debug.Log("Player");
            currentHealth -= damage;
            healthSlider.value = currentHealth;
        }
        else
        {
            currentHealth -= damage;
            Debug.Log("Enemy");
        }

        StopCoroutine(DamageFlash());
        StartCoroutine(DamageFlash());

        if (currentHealth <= 0)
        {
            isDead = true;
            Die();
        }
    }

    public void takeFireDamage(float damage, float statusChance)
    {
        float modifier = fireResistance / 100;
        float trueDamage = damage - (damage * modifier);
        takeDamage(trueDamage);
        float statusThreshold = Random.Range(1, 101);
        //Debug.Log("Threshold was " + statusThreshold + ", Status chance was " + statusChance);
        if (!isDead && canProcBurn && (statusThreshold - statusChance) <= 0)
        {
            StopCoroutine(ProcBurn());
            StartCoroutine(ProcBurn());
        }
    }

    public void takeIceDamage(float damage, float statusChance)
    {
        float modifier = iceResistance / 100;
        float trueDamage = damage - (damage * modifier);
        takeDamage(trueDamage);
        float statusThreshold = Random.Range(1, 101);
        //Debug.Log("Threshold was " + statusThreshold + ", Status chance was " + statusChance);
        if (!isDead && canProcFreeze && (statusThreshold - statusChance) <= 0)
        {
            StopCoroutine(ProcFreeze());
            StartCoroutine(ProcFreeze());
        }
    }

    public void takeLightningDamage(float damage, float statusChance)
    {
        float modifier = lightningResistance / 100;
        float trueDamage = damage - (damage * modifier);
        takeDamage(trueDamage);
        float statusThreshold = Random.Range(1, 101);
        //Debug.Log("Threshold was " + statusThreshold + ", Status chance was " + statusChance);
        if (!isDead && canProcShock && shockCooldown < 0 && (statusThreshold - statusChance) <= 0)
        {
            StopCoroutine(ProcShock());
            StartCoroutine(ProcShock());
        }
    }

    IEnumerator ProcBurn()
    {
        for (int i = 0; i < burnTicks; i++)
        {
            takeFireDamage(burnTickDamage, 0);
            yield return new WaitForSeconds(timeBtwBurnTicks);
        }
    }

    IEnumerator ProcFreeze()
    {
        for (int i = 0; i < freezeTicks; i++)
        {
            takeIceDamage(freezeTickDamage, 0);
            yield return new WaitForSeconds(timeBtwFreezeTicks);
        }
    }

    IEnumerator ProcShock()
    {
        for (int i = 0; i < shockTicks; i++)
        {
            takeLightningDamage(shockTickDamage, 0);
            yield return new WaitForSeconds(timeBtwShockTicks);
        }
        shockTimer = shockCooldown;
    }


    IEnumerator DamageFlash()
    {
        characterMesh.material.color = flashColor;
        yield return new WaitForSeconds(FlashTime);
        characterMesh.material.color = originalColor;
    }

    //checks to see if the monster/player is dead
    public bool IsThisDyingOrDead()
    {
        if (isDead)
            return true;

        if (currentHealth <= 0)
            return true;

        return false;
    }

    public void StartSinking()
    {
        if (gameObject.tag == "StoneMonster" || gameObject.tag == "Skeleton")
        {
            gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        }

        // Find the rigidbody component and make it kinematic (since we use Translate to sink the enemy).
        gameObject.GetComponent<Rigidbody>().isKinematic = true;

        // The enemy should no sink.
        isSinking = true;

        // After 2 seconds destory the enemy.
        Destroy(gameObject, 2f);
    }

    //kills the player/monster
    public virtual void Die()
    {
        StopAllCoroutines();

        //if the player is dying
        if (GetComponent<PlayerHP>() != null)
        {
            Destroy(gameObject, 5f);
        }
        //if the enemy is dying
        else if (GetComponent<enemyHP>() != null)
        {
            if (gameObject.tag != "StoneMonster" && gameObject.tag != "Skeleton" && gameObject.tag != "Skull" && gameObject.tag != "Turret")
            {
                anim.SetBool("Death", true);
                StartSinking();
            }
            else
            {
                StartSinking();
            }

            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Destroy(gameObject, 2f);
        }
        else if (GetComponent<BossHP>() != null)
        {
            if (gameObject.tag != "StoneMonster" && gameObject.tag != "Skeleton" && gameObject.tag != "Skull" && gameObject.tag != "Turret")
            {
                //anim.SetBool("Death", true);
                StartSinking();
            }
            else
            {
                StartSinking();
            }

            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Destroy(gameObject, 2f);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fireball")
        {
            if (gameObject.tag != "Player(Clone)")
            {
                Debug.Log("trigger" + currentPDamage);
                takeDamage(currentPDamage);
            }
        }
    }
}
